﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class WRDataGeneralHelper
    {
        public static List<WRDataGeneralModel> Covert(List<WRDataGeneral> entrys)
        {
            var models = entrys.ConvertAll(sc => new WRDataGeneralModel
            {
                  WRDNumber=sc.WRDNumber,
                  Ordinal=sc.Ordinal,
                  PalletID=sc.PalletID,
                  IDCode=sc.IDCode,
                  PartNumber=sc.PartNumber,
                  ProductFamily=sc.ProductFamily,
                  LotID=sc.LotID,
                  PartName=sc.PartName,
                  Unit=sc.Unit,
                  QuantityOrg=sc.QuantityOrg,
                  Quantity=sc.Quantity,
                  TotalGoodsOrg=sc.TotalGoodsOrg,
                  TotalGoods=sc.TotalGoods,
                  TotalQuantityOrg=sc.TotalQuantityOrg,
                  TotalQuantity=sc.TotalQuantity,
                  PackingVolume=sc.PackingVolume,
                  QuantityByPack=sc.QuantityByPack,
                  QuantityByItem=sc.QuantityByItem,
                  PackingQuantity=sc.PackingQuantity,
                  MFGDate=sc.MFGDate,
                  EXPDate=sc.EXPDate,
                  PONumber=sc.PONumber,
                  InputDate=sc.InputDate,
                  Remark=sc.Remark,
                  Status=sc.Status,
            });

            return models;
        }
    }
}
