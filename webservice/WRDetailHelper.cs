﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class WRDetailHelper
    {
        public static List<WRDetailModel> Covert(List<WRDetail> entrys)
        {
            var models = entrys.ConvertAll(sc => new WRDetailModel
            {
                  WRNumber=sc.WRNumber,
                  Ordinal=sc.Ordinal,
                  PalletID=sc.PalletID,
                  IDCode=sc.IDCode,
                  PartNumber=sc.PartNumber,
                  LotID=sc.LotID,
                  PartName=sc.PartName,
                  Unit=sc.Unit,
                  QuantityReceived=sc.QuantityReceived,
                  ReceiptQuantity=sc.ReceiptQuantity,
                  PackingQuantity=sc.PackingQuantity,
                  PackingVolume=sc.PackingVolume,
                  Status=sc.Status,
                  MFGDate=sc.MFGDate,
                  EXPDate=sc.EXPDate,
                  InputDate=sc.InputDate,
            });

            return models;
        }
    }
}
