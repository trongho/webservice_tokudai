﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webservice.Entities
{
    public class IOHeader
    {
        public string IONumber { get; set; }
        public string ModalityID { get; set; }
        public string ModalityName { get; set; }
        public DateTime? IODate { get; set; }
        public string WIRNumber { get; set; }
        public string WarehouseID { get; set; }
        public string WarehouseName { get; set; }
        public string HandlingStatusID { get; set; }
        public string HandlingStatusName { get; set; }
        public string BranchID { get; set; }
        public string BranchName { get; set; }
        public decimal? TotalQuantity { get; set; }
        public string Note { get; set; }
        public string Status { get; set; }
        public string CreatedUserID { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdatedUserID { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
