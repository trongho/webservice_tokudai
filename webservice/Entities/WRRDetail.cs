﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webservice.Entities
{
    public class WRRDetail
    {
      public string WRRNumber{get;set;}
      public int Ordinal{get;set;}
      public string PalletID{get;set;}
      public string PartNumber{get;set;}
      public string PartName{get;set;}
      public string ProductFamily{get;set;}
      public string LotID{get;set;}
      public decimal? QuantityByItem{get;set;}
      public decimal? QuantityByPack{get;set;}
      public decimal? PackingVolume{get;set;}
      public decimal? TotalQuantity{get;set;}
      public string Unit{get;set;}
      public DateTime? MFGDate{get;set;}
      public DateTime? EXPDate { get; set; }
      public string PONumber{get;set;}
      public string Remark{get;set;}
      public string Status{get;set;}
        public decimal? QuantityByPrinted { get; set; }
        public decimal? RemainingPrintQuantity { get; set; }
        public string IDCodeRange { get; set; }
    }
}
