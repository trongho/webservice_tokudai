﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webservice.Entities
{
    public class Material
    {
        public string MaterialID { get; set; }
        public string MaterialName { get; set; }
    }
}
