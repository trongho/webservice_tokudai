﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webservice.Entities
{
    public class TSDataGeneral
    {
      public string BranchID{get;set;}
      public string WarehouseID{get;set;}
      public string WarehouseName{get;set;}
      public DateTime? TallyDate{get;set;}
      public int Ordinal{get;set;}
      public string PartNumber{get;set;}
      public string PartName{get;set;}
      public string StockUnitID{get;set;}
      public decimal? TallyQuantity {get;set;}
      public decimal? LedgerQuantity {get;set;}
      public decimal? DifferenceQuantity {get;set;}
      public string Status{get;set;}
    }
}
