﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webservice.Entities
{
    public class WRDataDetail
    {
        public string WRDNumber { get; set; }
        public int Ordinal { get; set; }
        public string PalletID { get; set; }
        public string IDCode { get; set; }
        public decimal? QuantityOrg { get; set; }
        public decimal? Quantity { get; set; }
    }
}
