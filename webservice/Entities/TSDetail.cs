﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webservice.Entities
{
    public class TSDetail
    {
         public string TSNumber {get;set;}
         public int Ordinal {get;set;}
         public string PartNumber {get;set;}
         public string PartName {get;set;}
         public string TallyUnitID {get;set;}
         public decimal? UnitRate {get;set;}
         public string StockUnitID {get;set;}
        public decimal? PackingVolume { get; set; }
        public decimal? Quantity {get;set;}
        public decimal? QuantityByPack { get; set; }
        public string Note {get;set;}
         public string Status {get;set;}
    }
}
