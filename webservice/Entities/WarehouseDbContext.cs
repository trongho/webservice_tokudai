﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webservice.Entities
{
    public class WarehouseDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Right> Rights { get; set; }
        public DbSet<UserRight> UserRights { get; set; }
        public DbSet<GoodsLine> GoodsLines { get; set; }
        public DbSet<Screen> Screens { get; set; }
        public DbSet<UserRightOnScreen> UserRightOnScreens { get; set; }
        public DbSet<Report> Reports { get; set; }
        public DbSet<UserRightOnReport> UserRightOnReports { get; set; }
        public DbSet<Branch> Branchs { get; set; }
        public DbSet<Goods> Goods { get; set; }
        public DbSet<WRRHeader> WRRHeaders { get; set; }
        public DbSet<WRRDetail> WRRDetails { get; set; }
        public DbSet<WRHeader> WRHeaders { get; set; }
        public DbSet<WRDetail> WRDetails { get; set; }
        public DbSet<WRDataHeader> WRDataHeaders { get; set; }
        public DbSet<WRDataGeneral> WRDataGenerals { get; set; }
        public DbSet<WRDataDetail> WRDataDetails { get; set; }
        public DbSet<WIRHeader> WIRHeaders { get; set; }
        public DbSet<WIRDetail> WIRDetails { get; set; }
        public DbSet<IOHeader> IOHeaders { get; set; }
        public DbSet<IODetail> IODetails { get; set; }
        public DbSet<WIDataHeader> WIDataHeaders { get; set; }
        public DbSet<WIDataDetail> WIDataDetails { get; set; }
        public DbSet<WIDataGeneral> WIDataGenerals { get; set; }
        public DbSet<HandlingStatus> HandlingStatuss { get; set; }
        public DbSet<OrderStatus> OrderStatuss { get; set; }
        public DbSet<Modality> Modalitys { get; set; }
        public DbSet<Inventory>Inventorys { get; set; }
        public DbSet<InventoryOfLastMonths> InventoryOfLastMonthss { get; set; }
        public DbSet<Warehouse> Warehouses { get; set; }
        public DbSet<Color> Colors { get; set; }
        public DbSet<Unit> Units { get; set; }
        public DbSet<TallyData> TallyDatas { get; set; }
        public DbSet<TSHeader> TSHeaders { get; set; }
        public DbSet<TSDetail> TSDetails { get; set; }
        public DbSet<TSDataGeneral> TSDataGenerals { get; set; }
        public DbSet<GGDataHeader> GGDataHeaders { get; set; }
        public DbSet<GGDataDetail> GGDataDetails { get; set; }
        public DbSet<GGDataGeneral> GGDataGenerals { get; set; }
        public DbSet<Label> Labels { get; set; }
        public DbSet<InventoryDetail> InventoryDetails { get; set; }
        public DbSet<Material> Materials { get; set; }
        public DbSet<LabelPrint> LabelPrints { get; set; }
        public DbSet<InventoryOfLastMonthDetail> InventoryOfLastMonthDetails { get; set; }
        public DbSet<PalletHeader> PalletHeaders { get; set; }
        public DbSet<PalletDetail> PalletDetails { get; set; }
        public WarehouseDbContext(DbContextOptions<WarehouseDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<User>().ToTable("User");
            modelBuilder.Entity<Right>().ToTable("Right");
            modelBuilder.Entity<UserRight>().ToTable("UserRight").HasKey(e=>new { e.UserID,e.RightID });
            modelBuilder.Entity<GoodsLine>().ToTable("GoodsLine").HasKey(e => e.GoodsLineID);
            modelBuilder.Entity<Screen>().ToTable("Screen").HasKey(e => e.ScreenID);
            modelBuilder.Entity<UserRightOnScreen>().ToTable("UserRightOnScreen").HasKey(e => new { e.UserID, e.ScreenID });
            modelBuilder.Entity<Report>().ToTable("Report").HasKey(e => e.ReportID);
            modelBuilder.Entity<UserRightOnReport>().ToTable("UserRightOnReport").HasKey(e => new { e.UserID, e.ReportID });
            modelBuilder.Entity<Branch>().ToTable("Branch").HasKey(e => e.BranchID);

            modelBuilder.Entity<Goods>().ToTable("Goods").HasKey(e =>new { e.PartNumber,e.PalletID,e.Ordinal });
            modelBuilder.Entity<Goods>().Property(p =>p.Quantity).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<WRRHeader>().ToTable("WRRHeader").HasKey(e => e.WRRNumber);
            modelBuilder.Entity<WRRHeader>().Property(p => p.TotalQuantity).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<WRRDetail>().ToTable("WRRDetail").HasKey(e => new { e.WRRNumber, e.PalletID,e.Ordinal,e.LotID});
            modelBuilder.Entity<WRRDetail>().Property(p => p.PackingVolume).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRRDetail>().Property(p => p.TotalQuantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRRDetail>().Property(p => p.QuantityByItem).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRRDetail>().Property(p => p.QuantityByPack).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRRDetail>().Property(p => p.QuantityByPrinted).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRRDetail>().Property(p => p.RemainingPrintQuantity).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<WRHeader>().ToTable("WRHeader").HasKey(e => e.WRNumber);
            modelBuilder.Entity<WRHeader>().Property(p => p.TotalQuantity).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<WRDetail>().ToTable("WRDetail").HasKey(e => new { e.WRNumber, e.PalletID,e.Ordinal});
            modelBuilder.Entity<WRDetail>().Property(p => p.QuantityReceived).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDetail>().Property(p => p.ReceiptQuantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDetail>().Property(p => p.PackingQuantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDetail>().Property(p => p.PackingVolume).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<WRDataHeader>().ToTable("WRDataHeader").HasKey(e => e.WRDNumber);
            modelBuilder.Entity<WRDataHeader>().Property(p => p.TotalQuantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDataHeader>().Property(p => p.TotalQuantityOrg).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<WRDataGeneral>().ToTable("WRDataGeneral").HasKey(e => new { e.WRDNumber, e.PalletID,e.Ordinal,e.LotID});
            modelBuilder.Entity<WRDataGeneral>().Property(p => p.Quantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDataGeneral>().Property(p => p.TotalQuantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDataGeneral>().Property(p => p.TotalGoods).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDataGeneral>().Property(p => p.QuantityOrg).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDataGeneral>().Property(p => p.TotalQuantityOrg).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDataGeneral>().Property(p => p.TotalGoodsOrg).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDataGeneral>().Property(p => p.PackingVolume).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDataGeneral>().Property(p => p.QuantityByPack).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDataGeneral>().Property(p => p.QuantityByItem).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDataGeneral>().Property(p => p.PackingQuantity).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<WRDataDetail>().ToTable("WRDataDetail").HasKey(e => new { e.WRDNumber, e.PalletID, e.Ordinal });
            modelBuilder.Entity<WRDataDetail>().Property(p => p.Quantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDataDetail>().Property(p => p.QuantityOrg).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<HandlingStatus>().ToTable("HandlingStatus").HasKey(e => e.HandlingStatusID);

            modelBuilder.Entity<Warehouse>().ToTable("Warehouse").HasKey(e => e.WarehouseID);


            modelBuilder.Entity<OrderStatus>().ToTable("OrderStatus").HasKey(e => e.OrderStatusID);

            modelBuilder.Entity<Modality>().ToTable("Modality").HasKey(e => e.ModalityID);

            modelBuilder.Entity<WIRHeader>().ToTable("WIRHeader").HasKey(e => e.WIRNumber);
            modelBuilder.Entity<WIRHeader>().Property(p => p.TotalQuantity).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<WIRDetail>().ToTable("WIRDetail").HasKey(e => new { e.WIRNumber, e.PalletID,e.PartNumber,e.Ordinal});
            modelBuilder.Entity<WIRDetail>().Property(p => p.PackingVolume).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WIRDetail>().Property(p => p.TotalQuantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WIRDetail>().Property(p => p.QuantityByItem).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WIRDetail>().Property(p => p.QuantityByPack).HasColumnType("decimal(18,3)");


            modelBuilder.Entity<IOHeader>().ToTable("IOHeader").HasKey(e => e.IONumber);
            modelBuilder.Entity<IOHeader>().Property(p => p.TotalQuantity).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<IODetail>().ToTable("IODetail").HasKey(e => new { e.IONumber,e.PalletID,e.PartNumber, e.Ordinal });
            modelBuilder.Entity<IODetail>().Property(p => p.QuantityOrdered).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<IODetail>().Property(p => p.IssueQuantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<IODetail>().Property(p => p.PackingQuantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<IODetail>().Property(p => p.PackingVolume).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<WIDataHeader>().ToTable("WIDataHeader").HasKey(e => e.WIDNumber);
            modelBuilder.Entity<WIDataHeader>().Property(p => p.TotalQuantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WIDataHeader>().Property(p => p.TotalQuantityOrg).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<WIDataDetail>().ToTable("WIDataDetail").HasKey(e => new { e.WIDNumber, e.PartNumber, e.Ordinal,e.LotID,e.IDCode});
            modelBuilder.Entity<WIDataDetail>().Property(p => p.Quantity).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<WIDataGeneral>().ToTable("WIDataGeneral").HasKey(e => new { e.WIDNumber, e.PartNumber, e.Ordinal,e.PalletID});
            modelBuilder.Entity<WIDataGeneral>().Property(p => p.Quantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WIDataGeneral>().Property(p => p.TotalQuantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WIDataGeneral>().Property(p => p.TotalGoods).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WIDataGeneral>().Property(p => p.QuantityOrg).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WIDataGeneral>().Property(p => p.TotalQuantityOrg).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WIDataGeneral>().Property(p => p.TotalGoodsOrg).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WIDataGeneral>().Property(p => p.PackingVolume).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WIDataGeneral>().Property(p => p.QuantityByPack).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WIDataGeneral>().Property(p => p.QuantityByItem).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WIDataGeneral>().Property(p => p.PackingQuantity).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<Inventory>().ToTable("Inventory").HasKey(e => new { e.Year, e.Month, e.WarehouseID, e.PartNumber });
            modelBuilder.Entity<Inventory>().Property(p => p.OpeningStockQuantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<Inventory>().Property(p => p.ReceiptQuantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<Inventory>().Property(p => p.IssueQuantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<Inventory>().Property(p => p.ClosingStockQuantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<Inventory>().Property(p => p.PackingVolume).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<InventoryDetail>().ToTable("InventoryDetail").HasKey(e => new { e.Year, e.Month, e.WarehouseID, e.PartNumber,e.PalletID,e.IDCode });
            modelBuilder.Entity<InventoryDetail>().Property(p => p.Quantity).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<InventoryOfLastMonthDetail>().ToTable("InventoryOfLastMonthDetail").HasKey(e => new { e.Year, e.Month, e.WarehouseID, e.PartNumber, e.PalletID, e.IDCode });
            modelBuilder.Entity<InventoryOfLastMonthDetail>().Property(p => p.Quantity).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<InventoryOfLastMonths>().ToTable("InventoryOfLastMonths").HasKey(e => new { e.Year, e.Month, e.WarehouseID, e.PartNumber });
            modelBuilder.Entity<InventoryOfLastMonths>().Property(p => p.OpeningStockQuantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<InventoryOfLastMonths>().Property(p => p.ReceiptQuantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<InventoryOfLastMonths>().Property(p => p.IssueQuantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<InventoryOfLastMonths>().Property(p => p.ClosingStockQuantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<InventoryOfLastMonths>().Property(p => p.PackingVolume).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<Color>().ToTable("Color").HasKey(e => e.ColorID);

            modelBuilder.Entity<Unit>().ToTable("Unit").HasKey(e => e.UnitID);

            modelBuilder.Entity<TallyData>().ToTable("TallyData").HasKey(e => new { e.TSNumber, e.PartNumber, e.No });
            modelBuilder.Entity<TallyData>().Property(p => p.Quantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<TallyData>().Property(p => p.TotalQuantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<TallyData>().Property(p => p.TotalRow).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<TSHeader>().ToTable("TSHeader").HasKey(e => e.TSNumber);
            modelBuilder.Entity<TSHeader>().Property(p => p.TotalQuantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<TSHeader>().Property(p => p.TotalQuantityByPack).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<TSDetail>().ToTable("TSDetail").HasKey(e => new { e.TSNumber, e.Ordinal, e.PartNumber });
            modelBuilder.Entity<TSDetail>().Property(p => p.UnitRate).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<TSDetail>().Property(p => p.Quantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<TSDetail>().Property(p => p.QuantityByPack).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<TSDetail>().Property(p => p.PackingVolume).HasColumnType("decimal(18,3)");


            modelBuilder.Entity<TSDataGeneral>().ToTable("TSDataGeneral").HasKey(e => new { e.WarehouseID, e.TallyDate, e.Ordinal,e.PartNumber});
            modelBuilder.Entity<TSDataGeneral>().Property(p => p.TallyQuantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<TSDataGeneral>().Property(p => p.LedgerQuantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<TSDataGeneral>().Property(p => p.DifferenceQuantity).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<GGDataHeader>().ToTable("GGDataHeader").HasKey(e => e.GGDNumber);
            modelBuilder.Entity<GGDataHeader>().Property(p => p.TotalQuantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<GGDataHeader>().Property(p => p.TotalQuantityOrg).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<GGDataDetail>().ToTable("GGDataDetail").HasKey(e => new { e.GGDNumber, e.GoodsID, e.Ordinal });
            modelBuilder.Entity<GGDataDetail>().Property(p => p.Quantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<GGDataDetail>().Property(p => p.TotalQuantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<GGDataDetail>().Property(p => p.TotalGoods).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<GGDataDetail>().Property(p => p.QuantityOrg).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<GGDataDetail>().Property(p => p.TotalQuantityOrg).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<GGDataDetail>().Property(p => p.TotalGoodsOrg).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<GGDataDetail>().Property(p => p.PackingVolume).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<GGDataDetail>().Property(p => p.QuantityByPack).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<GGDataDetail>().Property(p => p.QuantityByItem).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<GGDataGeneral>().ToTable("GGDataGeneral").HasKey(e => new { e.GGDNumber, e.GoodsID, e.Ordinal });
            modelBuilder.Entity<GGDataGeneral>().Property(p => p.Quantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<GGDataGeneral>().Property(p => p.TotalQuantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<GGDataGeneral>().Property(p => p.TotalGoods).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<GGDataGeneral>().Property(p => p.QuantityOrg).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<GGDataGeneral>().Property(p => p.TotalQuantityOrg).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<GGDataGeneral>().Property(p => p.TotalGoodsOrg).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<GGDataGeneral>().Property(p => p.PackingVolume).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<GGDataGeneral>().Property(p => p.QuantityByPack).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<GGDataGeneral>().Property(p => p.QuantityByItem).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<Label>().ToTable("Label").HasKey(e => new { e.PalletID });

            modelBuilder.Entity<Material>().ToTable("Material").HasKey(e => new { e.MaterialID });

            modelBuilder.Entity<LabelPrint>().ToTable("LabelPrint").HasKey(e => new { e.WRRNumber, e.PalletID, e.Ordinal, e.LotID });
            modelBuilder.Entity<LabelPrint>().Property(p => p.Quantity).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<PalletHeader>().ToTable("PalletHeader").HasKey(e => new { e.PalletID, e.Ordinal});
            modelBuilder.Entity<PalletHeader>().Property(p => p.TotalIDCode).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<PalletDetail>().ToTable("PalletDetail").HasKey(e => new { e.PalletID, e.Ordinal,e.IDCode });
            modelBuilder.Entity<PalletDetail>().Property(p => p.Quantity).HasColumnType("decimal(18,3)");

        }

    }
}
