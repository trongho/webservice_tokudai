﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webservice.Entities
{
    public class LabelPrint
    {
        public string WRRNumber { get; set; }
        public int Ordinal { get; set; }
        public string PalletID { get; set; }
        public string PartNumber { get; set; }
        public string PartName { get; set; }
        public string ProductFamily { get; set; }
        public string LotID { get; set; }
        public decimal? Quantity { get; set; }
        public string Unit { get; set; }
        public DateTime? MFGDate { get; set; }
        public DateTime? EXPDate { get; set; }
        public string IDCode { get; set; }
        public string Status { get; set; }
    }
}
