﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class WIDataGeneralHelper
    {
        public static List<WIDataGeneralModel> Covert(List<WIDataGeneral> entrys)
        {
            var models = entrys.ConvertAll(sc => new WIDataGeneralModel
            {
                WIDNumber = sc.WIDNumber,
                Ordinal = sc.Ordinal,
                PalletID = sc.PalletID,
                IDCode = sc.IDCode,
                PartNumber = sc.PartNumber,
                ProductFamily = sc.ProductFamily,
                LotID = sc.LotID,
                PartName = sc.PartName,
                QuantityOrg = sc.QuantityOrg,
                Quantity = sc.Quantity,
                TotalGoodsOrg = sc.TotalGoodsOrg,
                TotalGoods = sc.TotalGoods,
                TotalQuantityOrg = sc.TotalQuantityOrg,
                TotalQuantity = sc.TotalQuantity,
                PackingVolume = sc.PackingVolume,
                QuantityByPack = sc.QuantityByPack,
                QuantityByItem = sc.QuantityByItem,
                PackingQuantity = sc.PackingQuantity,
                MFGDate = sc.MFGDate,
                EXPDate = sc.EXPDate,
                InputDate=sc.InputDate,
                PONumber = sc.PONumber,
                OutputDate = sc.OutputDate,
                Remark = sc.Remark,
                Status = sc.Status,
                Material = sc.Material,
                Mold = sc.Mold,
                Unit=sc.Unit
            });

            return models;
        }
    }
}
