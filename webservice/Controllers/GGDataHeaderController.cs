﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class GGDataHeaderController : ControllerBase
    {
        private readonly IGGDataHeaderService service;

        public GGDataHeaderController(IGGDataHeaderService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Get()
        {
            var entrys = await service.GetAll();
            if (entrys == null)
            {
                return NotFound();
            }
            var entryModels = GGDataHeaderHelper.Covert(entrys);
            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByID/{wRDNumber}")]
        public async Task<IActionResult> GetGGDataHeaderUnderWRDNumber(String wRDNumber)
        {
            var entrys = await service.GetUnderId(wRDNumber);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = GGDataHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> Create([FromBody] GGDataHeader GGDataHeader)
        {
            var entry = await service.Create(GGDataHeader);

            return CreatedAtAction(
                 nameof(Get), new { WRRNumber = GGDataHeader.GGDNumber }, entry);
        }

        [HttpPut]
        [Route("Put/{WRDNumber}")]
        public async Task<IActionResult> Update(String wRDNumber, [FromBody] GGDataHeader GGDataHeader)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (wRDNumber != GGDataHeader.GGDNumber)
            {
                return BadRequest();
            }

            await service.Update(wRDNumber, GGDataHeader);

            return new NoContentResult();
        }

        [HttpDelete]
        [Route("Delete/{wRDNumber}")]
        public async Task<IActionResult> Delete(String wRDNumber)
        {
            var entry = await service.Delete(wRDNumber);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }

        [HttpGet]
        [Route("CheckExist/{wRDNumber}/")]
        public async Task<IActionResult> checkExist(String wRDNumber)
        {
            var result = await service.checkExist(wRDNumber);
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpGet]
        [Route("lastWRDNumber")]
        public async Task<IActionResult> GetLastWRDNumber()
        {
            var WRDNumber = await service.GetLastId();
            if (WRDNumber == null)
            {
                return NotFound();
            }

            return Ok(WRDNumber);
        }

        [HttpGet]
        [Route("FilterByDate/{fromDate}/{toDate}")]
        public async Task<IActionResult> GetGGDataHeaderUnderDate(DateTime fromDate, DateTime toDate)
        {
            var entrys = await service.GetUnderDate(fromDate, toDate);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = GGDataHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("FilterByBranch/{branchID}")]
        public async Task<IActionResult> GetGGDataHeaderUnderBranchID(String branchID)
        {
            var entrys = await service.GetUnderBranchID(branchID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = GGDataHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("FilterByHandlingStatus/{handlingStatusID}")]
        public async Task<IActionResult> GetWRRHeaderUnderHandlingStatusID(String handlingStatusID)
        {
            var entrys = await service.GetUnderHandlingStatusID(handlingStatusID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = GGDataHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("FilterByOrderStatus/{orderStatusID}")]
        public async Task<IActionResult> GetGGDataHeaderUnderOrderStatusID(String orderStatusID)
        {
            var entrys = await service.GetUnderOrderStatusID(orderStatusID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = GGDataHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }
    }
}
