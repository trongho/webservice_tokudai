﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class LabelController : Controller
    {
        private readonly ILabelService LabelService;

        public LabelController(ILabelService LabelService)
        {
            this.LabelService = LabelService;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Get()
        {
            var entrys = await LabelService.GetAll();
            if (entrys == null)
            {
                return NotFound();
            }
            var entryModels = LabelHelper.Covert(entrys);
            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByParams/{palletID}")]
        public async Task<IActionResult> GetUnderID(String palletID)
        {
            var entrys = await LabelService.GetUnderId(palletID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = LabelHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> Create([FromBody] Label Label)
        {
            var entry = await LabelService.Create(Label);

            return CreatedAtAction(
                 nameof(Get), new { LabelID = Label.PalletID}, entry);
        }

        [HttpPut]
        [Route("Put/{palletID}")]
        public async Task<IActionResult> Update(String palletID, [FromBody] Label Label)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (palletID != Label.PalletID)
            {
                return BadRequest();
            }

            await LabelService.Update(palletID, Label);

            return new NoContentResult();
        }

        [HttpDelete]
        [Route("Delete/{palletID}")]
        public async Task<IActionResult> Delete(String palletID)
        {
            var entry = await LabelService.Delete(palletID);
            return Ok(entry);
        }

        [HttpDelete]
        [Route("DeleteALl")]
        public async Task<IActionResult> DeleteAll()
        {
            var entry = await LabelService.Delete();
            return Ok(entry);
        }
    }
}
