﻿using DevExpress.CodeParser;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class WIRDetailController : ControllerBase
    {
        private readonly IWIRDetailService service;

        public WIRDetailController(IWIRDetailService service)
        {
            this.service = service;
        }

       

        [HttpGet]
        [Route("GetByID/{WIRNumber}")]
        public async Task<IActionResult> GetWRRDetailUnderWIRNumber(String wIRNumber)
        {
            var entrys = await service.GetUnderId(wIRNumber);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WIRDetailHelper.Covert(entrys);

            return Ok(entryModels);
        }

      
        
        [HttpGet]
        [Route("GetByParams3/{WIRNumber}/{palletID}/{partNumber}")]
        public async Task<IActionResult> GetByWIRNumberAndPalletID(String wIRNumber, String palletID,String partNumber)
        {
            var entrys = await service.GetUnderId(wIRNumber, palletID,partNumber);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WIRDetailHelper.Covert(entrys);

            return Ok(entryModels);
        }


        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> Create([FromBody] WIRDetail wIRDetail)
        {
            var entry = await service.Create(wIRDetail);

            return CreatedAtAction(
                 nameof(Get), new { WRRNumber = wIRDetail.WIRNumber }, entry);
        }


        [HttpPut]
        [Route("Put/{WIRNumber}/{palletID}/{partNumber}/{ordinal}")]
        public async Task<IActionResult> Update(String wIRNumber, String palletID,String partNumber,int ordinal,[FromBody] WIRDetail wIRDetail)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (wIRNumber != wIRDetail.WIRNumber)
            {
                return BadRequest();
            }

            await service.Update(wIRNumber, palletID,partNumber,ordinal,wIRDetail);

            return new NoContentResult();
        }

        [HttpDelete]
        [Route("Delete/{wIRNumber}")]
        public async Task<IActionResult> Delete(String wIRNumber)
        {
            var entry = await service.Delete(wIRNumber);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }
        [HttpDelete]
        [Route("DeleteByParams4/{wRRNumber}/{palletID}/{partNumber}/{ordinal}")]
        public async Task<IActionResult> DeleteByParams4(String wRRNumber, String palletID, String partNumber,int ordinal)
        {
            var entry = await service.Delete(wRRNumber, palletID,partNumber,ordinal);
            return Ok(entry);
        }
    }
}
