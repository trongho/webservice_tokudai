﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Controllers
{
    [ApiController]
    //[Produces("application/json")]
    [Route("api/[controller]")]
    public class GoodsLineController: ControllerBase
    {
        private readonly IGoodsLineService goodsLineService;

        public GoodsLineController(IGoodsLineService goodsLineService)
        {
            this.goodsLineService = goodsLineService;
        }

        [HttpGet]
        [Route("ImportGoodsLine")]
        public async Task<IActionResult> ImportGoodsLine()
        {
            var goodsLines =goodsLineService.ImportGoodsLine();
            if (goodsLines == null)
            {
                return NotFound();
            }

            var goodsLineModels = GoodsLineHelper.CovertGoodsLines(goodsLines);

            return Ok(goodsLineModels);
        }

        [HttpGet]
        [Route("ExportGoodsLine")]
        public async Task<IActionResult> ExportGoodsLine()
        {
            var result = goodsLineService.ExportGoodsLine();
            if (result == null)
            {
                return NotFound();
            }
            return Ok(result);
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Get()
        {
            var goodsLines = await goodsLineService.GetAllGoodsLine();
            if (goodsLines == null)
            {
                return NotFound();
            }

            var goodsLineModels = GoodsLineHelper.CovertGoodsLines(goodsLines);

            return Ok(goodsLineModels);
        }

        [HttpGet]
        [Route("{goodsLineId}")]
        public async Task<IActionResult> GetGoodsLineUnderId(String goodsLineId)
        {
            var goodsLine = await goodsLineService.GetGoodsLineUnderId(goodsLineId);
            if (goodsLine == null)
            {
                return NotFound();
            }
            return Ok(goodsLine);
        }

        [HttpPost]
        [Route("PostGoodsLine")]
        public async Task<IActionResult> CreateGoodsLine([FromBody] GoodsLine goodsLine)
        {
            var goodsLines = await goodsLineService.CreateGoodsLine(goodsLine);

            return CreatedAtAction(
                 nameof(Get), new { id = goodsLine.GoodsLineID }, goodsLine);
        }

        [HttpPut]
        [Route("PutGoodsLine/{goodsLineId}")]
        public async Task<IActionResult> UpdateGoodsLine(String goodsLineId, [FromBody] GoodsLine goodsLine)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (goodsLineId != goodsLine.GoodsLineID)
            {
                return BadRequest();
            }

            await goodsLineService.UpdateGoodsLine(goodsLineId, goodsLine);

            return Ok();
        }

        [HttpDelete]
        [Route("DeleteGoodsLine/{goodsLineId}")]
        public async Task<IActionResult> DeleteGoodsLine(String goodsLineId)
        {
            var goodsLines =goodsLineService.DeleteGoodsLine(goodsLineId);
            if (goodsLines == null)
            {
                return NotFound();
            }

            return Ok(goodsLines);
        }

        [HttpGet]
        [Route("CheckExist/{goodsLineId}")]
        public async Task<IActionResult> checkExist(String goodsLineId)
        {
            var result = await goodsLineService.checkExist(goodsLineId);
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }
    }
}
