﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class WRDataGeneralController : ControllerBase
    {
        private readonly IWRDataGeneralService service;

        public WRDataGeneralController(IWRDataGeneralService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Get()
        {
            var entrys = await service.GetAll();
            if (entrys == null)
            {
                return NotFound();
            }
            var entryModels = WRDataGeneralHelper.Covert(entrys);
            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByID/{WRDNumber}")]
        public async Task<IActionResult> GetWRDataGeneralUnderWRDNumber(String wRDNumber)
        {
            var entrys = await service.GetUnderId(wRDNumber);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WRDataGeneralHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByParams/{wrdNumber}/{palletID}/{ordinal}")]
        public async Task<IActionResult> GetByParams(String wRDNumber, String palletID, int ordinal)
        {
            var entrys = await service.GetUnderId(wRDNumber,palletID, ordinal);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WRDataGeneralHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByParams3/{wrdNumber}/{palletID}/{lotID}")]
        public async Task<IActionResult> GetByParams3(String wRDNumber, String palletID, String lotID)
        {
            var entrys = await service.GetUnderId(wRDNumber, palletID, lotID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WRDataGeneralHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByParams4/{wrdNumber}/{palletID}/{ordinal}/{lotID}")]
        public async Task<IActionResult> GetByParams4(String wRDNumber, String palletID, int ordinal,String lotID)
        {
            var entrys = await service.GetUnderId(wRDNumber, palletID, ordinal,lotID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WRDataGeneralHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByWRDNumberAndPalletID/{WRDNumber}/{palletID}")]
        public async Task<IActionResult> GetByWRDNumberAndPalletID(String wRDNumber, String palletID)
        {
            var entrys = await service.GetUnderId(wRDNumber, palletID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WRDataGeneralHelper.Covert(entrys);

            return Ok(entryModels);
        }


        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> Create([FromBody] WRDataGeneral WRDataGeneral)
        {
            var entry = await service.Create(WRDataGeneral);

            return CreatedAtAction(
                 nameof(Get), new WRDataGeneral { WRDNumber = WRDataGeneral.WRDNumber }, entry);
        }


        [HttpPut]
        [Route("Put/{WRDNumber}/{palletID}/{ordinal}/{lotID}")]
        public async Task<IActionResult> Update(String wRDNumber,String palletID,int ordinal,String lotID, [FromBody] WRDataGeneral WRDataGeneral)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (wRDNumber != WRDataGeneral.WRDNumber)
            {
                return BadRequest();
            }

            await service.Update(wRDNumber,palletID,ordinal,lotID, WRDataGeneral);

            return new NoContentResult();
        }

        [HttpDelete]
        [Route("Delete/{wRDNumber}")]
        public async Task<IActionResult> Delete(String wRDNumber)
        {
            var entry = await service.Delete(wRDNumber);
            return Ok(entry);
        }

        [HttpDelete]
        [Route("Delete3/{wRDNumber}/{palletID}/{lotID}")]
        public async Task<IActionResult> Delete3(String wRDNumber,String palletID,String lotID)
        {
            var entry = await service.Delete(wRDNumber,palletID,lotID);
            return Ok(entry);
        }

        [HttpDelete]
        [Route("Delete2/{wRDNumber}/{palletID}")]
        public async Task<IActionResult> Delete2(String wRDNumber, String palletID)
        {
            var entry = await service.Delete(wRDNumber, palletID);
            return Ok(entry);
        }
    }
}
