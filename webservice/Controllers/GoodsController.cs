﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class GoodsController : ControllerBase
    {
        private readonly IGoodsService goodsService;

        public GoodsController(IGoodsService goodsService)
        {
            this.goodsService = goodsService;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Get()
        {
            var entrys = await goodsService.GetAllGoods();
            if (entrys == null)
            {
                return NotFound();
            }
            var entryModels = GoodsHelper.Covert(entrys);
            return Ok(entryModels);
        }
        [HttpGet]
        [Route("GetByPalletID/{palletID}")]
        public async Task<IActionResult> GetUnderID(String palletID)
        {
            var entrys = await goodsService.GetUnderId(palletID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = GoodsHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByPartNumber/{partNumber}")]
        public async Task<IActionResult> GetUnderID2(String partNumber)
        {
            var entrys = await goodsService.GetUnderId2(partNumber);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = GoodsHelper.Covert(entrys);

            return Ok(entryModels);
        }


        [HttpGet]
        [Route("GetByParams/{partNumber}/{palletID}")]
        public async Task<IActionResult> GetUnderID(String partNumber, String palletID)
        {
            var entrys = await goodsService.GetUnderId(partNumber, palletID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = GoodsHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> Create([FromBody] Goods goods)
        {
            var entry = await goodsService.Create(goods);

            return CreatedAtAction(
                 nameof(Get), new { GoodsID = goods.PartNumber }, entry);
        }

        [HttpPut]
        [Route("Put/{partNumber}/{palletID}")]
        public async Task<IActionResult> Update(String partNumber, String palletID, [FromBody] Goods goods)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (partNumber != goods.PartNumber||palletID!=goods.PalletID)
            {
                return BadRequest();
            }

            await goodsService.Update(partNumber,palletID, goods);

            return new NoContentResult();
        }

        [HttpDelete]
        [Route("Delete/{partNumber}/{palletID}")]
        public async Task<IActionResult> Delete(String partNumber,String palletID)
        {
            var entry = await goodsService.Delete(partNumber,palletID);
            return Ok(entry);
        }

        [HttpDelete]
        [Route("DeleteALl")]
        public async Task<IActionResult> DeleteAll()
        {
            var entry = await goodsService.Delete();
            return Ok(entry);
        }

    }
}
