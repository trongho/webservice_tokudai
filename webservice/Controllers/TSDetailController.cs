﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TSDetailController : ControllerBase
    {
        private readonly ITSDetailService service;

        public TSDetailController(ITSDetailService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Route("GetByID/{id}")]
        public async Task<IActionResult> GetUnderID(String id)
        {
            var entrys = await service.GetUnderId(id);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = TSDetailHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByMultiID/{TSNumber}/{GoodsID}/{Ordinal}")]
        public async Task<IActionResult> GetUnderMultiId(String TSNumber, String goodsID, int ordinal)
        {
            var entrys = await service.GetMultiId(TSNumber, goodsID, ordinal);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = TSDetailHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByTSNumberAndPartNumber/{TSNumber}/{PartNumber}")]
        public async Task<IActionResult> GetUnderMultiId(String TSNumber, String PartNumber)
        {
            var entrys = await service.GetUnderId(TSNumber, PartNumber);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = TSDetailHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> Create([FromBody] TSDetail entry)
        {
            var entrys = await service.Create(entry);

            return CreatedAtAction(
                 nameof(GetUnderID), new { TSNumber = entry.TSNumber }, entry);
        }

        [HttpPut]
        [Route("Put/{TSNumber}/{GoodsID}/{Ordinal}")]
        public async Task<IActionResult> Update(String TSNumber,String GoodsID,int Ordinal, [FromBody] TSDetail entry)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (TSNumber != entry.TSNumber)
            {
                return BadRequest();
            }

            await service.Update(TSNumber,Ordinal,GoodsID, entry);

            return new NoContentResult();
        }

        [HttpDelete]
        [Route("Delete/{id}")]
        public async Task<IActionResult> Delete(String id)
        {
            var entry = await service.Delete(id);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }

        [HttpDelete]
        [Route("DeleteDetail/{TSNumber}/{GoodsID}/{Ordinal}")]
        public async Task<IActionResult> DeleteDetail(String TSNumber,String GoodsID, int Ordinal)
        {
            var entry = await service.Delete(TSNumber,Ordinal,GoodsID);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }

    }
}
