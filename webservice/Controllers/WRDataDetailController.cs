﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Interfaces;

namespace webservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class WRDataDetailController : Controller
    {
        private readonly IWRDataDetailService service;

        public WRDataDetailController(IWRDataDetailService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Route("GetByWRDNumberAndPalletID/{WRDNumber}/{palletID}")]
        public async Task<IActionResult> GetByWRDNumberAndPalletID(String wRDNumber, String palletID)
        {
            var entrys = await service.GetUnderId(wRDNumber, palletID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WRDataDetailHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByID/{WRDNumber}")]
        public async Task<IActionResult> GetByID(String wRDNumber)
        {
            var entrys = await service.GetUnderId(wRDNumber);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WRDataDetailHelper.Covert(entrys);

            return Ok(entryModels);
        }
    }
}
