﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Win32.SafeHandles;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace webservice.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class PrinterController : ControllerBase
    {
        [DllImport("kernel32.dll", SetLastError = true)]
        static extern SafeFileHandle CreateFile(string lpFileName, FileAccess dwDesiredAccess,
        uint dwShareMode, IntPtr lpSecurityAttributes, FileMode dwCreationDisposition,
        uint dwFlagsAndAttributes, IntPtr hTemplateFile);

        [HttpPost]
        [Route("Post")]
        public IActionResult Post([FromBody]string zplString)
        {
            var result = zplString;
            byte[] bytes = Encoding.ASCII.GetBytes(result);
            Print(bytes);
            return Ok(result);
        }

        private void Print(byte[] command)
        {

            Helpers.RawPrinterHelper.SendFileToPrinter(command);
        }
    }
}
