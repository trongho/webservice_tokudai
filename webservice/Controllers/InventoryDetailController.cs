﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class InventoryDetailController : Controller
    {
        private readonly IInventoryDetailService service;

        public InventoryDetailController(IInventoryDetailService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Get()
        {
            var entrys = await service.GetAll();
            if (entrys == null)
            {
                return NotFound();
            }
            var entryModels = InventoryDetailHelper.Covert(entrys);
            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByIDCode/{idCode}")]
        public async Task<IActionResult> GetByIDCode(string idCode)
        {
            var entrys = await service.GetUnderIDCode(idCode);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = InventoryDetailHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByPartNumber/{PartNumber}")]
        public async Task<IActionResult> GetByPartNumber(string partNumber)
        {
            var entrys = await service.GetUnderPartNumber(partNumber);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = InventoryDetailHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByWarehouseID/{WareHouseID}")]
        public async Task<IActionResult> GetByWarehouseID(string warehouseID)
        {
            var entrys = await service.GetUnderId(warehouseID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = InventoryDetailHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByParams3/{Year}/{Month}/{WareHouseID}")]
        public async Task<IActionResult> GetByParams3(Int16 year, Int16 month, String warehouseID)
        {
            var entrys = await service.GetUnderId(year, month, warehouseID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = InventoryDetailHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByParams4/{Year}/{Month}/{WareHouseID}/{PartNumber}")]
        public async Task<IActionResult> GetByParams4(Int16 year, Int16 month, String warehouseID, String PartNumber)
        {
            var entrys = await service.GetUnderId(year, month, warehouseID, PartNumber);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = InventoryDetailHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByWarehouseAndPallet/{Year}/{Month}/{WareHouseID}/{PalletID}")]
        public async Task<IActionResult> GetByWarehouseAndPallet(Int16 year, Int16 month, String warehouseID, String PalletID)
        {
            var entrys = await service.GetUnderWarehouseAnPallet(year, month, warehouseID,PalletID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = InventoryDetailHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByParams5/{Year}/{Month}/{WareHouseID}/{PartNumber}/{PalletID}")]
        public async Task<IActionResult> GetByParams5(Int16 year, Int16 month, String warehouseID, String PartNumber,String PalletID)
        {
            var entrys = await service.GetUnderId(year, month, warehouseID, PartNumber,PalletID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = InventoryDetailHelper.Covert(entrys);

            return Ok(entryModels);
        }


        [HttpGet]
        [Route("GetByParams6/{Year}/{Month}/{WareHouseID}/{PartNumber}/{PalletID}/{IDCode}")]
        public async Task<IActionResult> GetByParams6(Int16 year, Int16 month, String warehouseID, String PartNumber, String PalletID,String IDCode)
        {
            var entrys = await service.GetUnderId2(year, month, warehouseID, PartNumber, PalletID,IDCode);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = InventoryDetailHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByParams62/{Year}/{Month}/{WareHouseID}/{PartNumber}/{PalletID}/{LotID}")]
        public async Task<IActionResult> GetByParams62(Int16 year, Int16 month, String warehouseID, String PartNumber, String PalletID, String LotID)
        {
            var entrys = await service.GetUnderId(year, month, warehouseID, PartNumber, PalletID, LotID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = InventoryDetailHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("CheckExist/{WareHouseID}/{PartNumber}/{PalletID}/{IDCode}")]
        public async Task<IActionResult> CheckExist(String warehouseID, String PartNumber, String PalletID, String IDCode)
        {
            var entrys = await service.GetUnderId(warehouseID, PartNumber, PalletID, IDCode);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = InventoryDetailHelper.Covert(entrys);

            return Ok(entryModels);
        }


        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> Create([FromBody] InventoryDetail inventory)
        {
            var entry = await service.Create(inventory);

            return CreatedAtAction(
                 nameof(Get), new { Year = inventory.Year, Month = inventory.Month, WarehouseID = inventory.WarehouseID, PartNumber = inventory.PartNumber,PalletID=inventory.PalletID}, entry);
        }


        [HttpPut]
        [Route("Put/{Year}/{Month}/{WareHouseID}/{PartNumber}/{PalletID}")]
        public async Task<IActionResult> Update(Int16 year, Int16 month, String warehouseID, String PartNumber,String PalletID, [FromBody]InventoryDetail inventory)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (year != inventory.Year && month != inventory.Month && warehouseID != inventory.WarehouseID && PartNumber != inventory.PartNumber&&PalletID!=inventory.PalletID)
            {
                return BadRequest();
            }

            await service.Update(year, month, warehouseID, PartNumber,PalletID, inventory);

            return new NoContentResult();
        }

        [HttpDelete]
        [Route("Delete3/{Year}/{Month}/{WareHouseID}")]
        public async Task<IActionResult> Delete3(Int16 year, Int16 month, String warehouseID)
        {
            var entry = await service.Delete(year, month, warehouseID);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }

        [HttpDelete]
        [Route("Delete4/{Year}/{Month}/{WareHouseID}/{PartNumber}")]
        public async Task<IActionResult> Delete4(Int16 year, Int16 month, String warehouseID, String PartNumber)
        {
            var entry = await service.Delete(year, month, warehouseID, PartNumber);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }
        [HttpDelete]
        [Route("Delete5/{Year}/{Month}/{WareHouseID}/{PartNumber}/{PalletID}")]
        public async Task<IActionResult> Delete5(Int16 year, Int16 month, String warehouseID, String PartNumber,String PalletID)
        {
            var entry = await service.Delete(year, month, warehouseID, PartNumber,PalletID);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }

        [HttpDelete]
        [Route("Delete6/{Year}/{Month}/{WareHouseID}/{PartNumber}/{PalletID}/{IDCode}")]
        public async Task<IActionResult> Delete6(Int16 year, Int16 month, String warehouseID, String PartNumber, String PalletID,String IDCode)
        {
            var entry = await service.Delete(year, month, warehouseID, PartNumber, PalletID,IDCode);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }


        [HttpDelete]
        [Route("DeleteOldDate/{WareHouseID}")]
        public async Task<IActionResult> DeleteOldDate(String warehouseID)
        {
            var entry = await service.DeleteOldDate(warehouseID);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }

    }
}
