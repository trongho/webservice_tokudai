﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PalletHeaderController : ControllerBase
    {
        private readonly IPalletHeaderService service;

        public PalletHeaderController(IPalletHeaderService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Get()
        {
            var entrys = await service.GetAll();
            if (entrys == null)
            {
                return NotFound();
            }
            var entryModels = PalletHeaderHelper.Covert(entrys);
            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByID/{palletID}")]
        public async Task<IActionResult> GetByID(String palletID)
        {
            var entrys = await service.GetUnderId(palletID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = PalletHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByCreatedDate/{fromDate}/{toDate}")]
        public async Task<IActionResult> GetByCreatedDate(DateTime fromDate,DateTime toDate)
        {
            var entrys = await service.GetUnderCreatedDate(fromDate, toDate);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = PalletHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }


        [HttpGet]
        [Route("GetByStatus/{status}")]
        public async Task<IActionResult> GetByStatus(String status)
        {
            var entrys = await service.GetUnderStatus(status);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = PalletHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpPost]
        [Route("Post")]
        public IActionResult Create([FromBody] PalletHeader PalletHeader)
        {
            var entry = service.Create(PalletHeader);

            return CreatedAtAction(
                 nameof(Get), new {PalletID = PalletHeader.PalletID }, entry);
        }

        [HttpPut]
        [Route("Put/{palletID}/{ordinal}")]
        public IActionResult Update(String palletID,int ordinal, [FromBody] PalletHeader PalletHeader)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (palletID != PalletHeader.PalletID)
            {
                return BadRequest();
            }

            service.Update(palletID,ordinal, PalletHeader);

            return new NoContentResult();
        }

        [HttpDelete]
        [Route("Delete/{palletID}")]
        public IActionResult Delete(String palletID)
        {
            var entry = service.Delete(palletID);

            return Ok(entry);
        }

        [HttpGet]
        [Route("GetLastID/{str}")]
        public IActionResult GetLastID(string str)
        {
            var WRDNumber = service.GetLastId(str);
            if (WRDNumber == null)
            {
                return NotFound();
            }

            return Ok(WRDNumber);
        }

    }
}
