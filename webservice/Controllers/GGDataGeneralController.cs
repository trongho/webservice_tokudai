﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class GGDataGeneralController : ControllerBase
    {
        private readonly IGGDataGeneralService service;

        public GGDataGeneralController(IGGDataGeneralService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Get()
        {
            var entrys = await service.GetAll();
            if (entrys == null)
            {
                return NotFound();
            }
            var entryModels = GGDataGeneralHelper.Covert(entrys);
            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByID/{GGDNumber}")]
        public async Task<IActionResult> GetGGDataGeneralUnderGGDNumber(String GGDNumber)
        {
            var entrys = await service.GetUnderId(GGDNumber);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = GGDataGeneralHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByMultiID/{GGDNumber}/{goodsID}/{ordinal}")]
        public async Task<IActionResult> GetUnderMultiId(String GGDNumber, String goodsID, int ordinal)
        {
            var entrys = await service.GetUnderId(GGDNumber, goodsID, ordinal);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = GGDataGeneralHelper.Covert(entrys);

            return Ok(entryModels);
        }


        [HttpGet]
        [Route("GetByGGDNumberAndGoodsID/{GGDNumber}/{goodsID}")]
        public async Task<IActionResult> GetUnderMultiGoodsID(String GGDNumber, String goodsID)
        {
            var entrys = await service.GetUnderId(GGDNumber, goodsID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = GGDataGeneralHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> Create([FromBody] GGDataGeneral GGDataGeneral)
        {
            var entry = await service.Create(GGDataGeneral);

            return CreatedAtAction(
                 nameof(Get), new { GGDNumber = GGDataGeneral.GGDNumber }, entry);
        }


        [HttpPut]
        [Route("Put/{GGDNumber}/{goodsID}/{ordinal}")]
        public async Task<IActionResult> Update(String GGDNumber, String goodsID, int ordinal, [FromBody] GGDataGeneral GGDataGeneral)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (GGDNumber != GGDataGeneral.GGDNumber)
            {
                return BadRequest();
            }

            await service.Update(GGDNumber, goodsID, ordinal, GGDataGeneral);

            return new NoContentResult();
        }

        [HttpDelete]
        [Route("Delete/{GGDNumber}")]
        public async Task<IActionResult> Delete(String GGDNumber)
        {
            var entry = await service.Delete(GGDNumber);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }

        [HttpGet]
        [Route("CheckExist/{GGDNumber}/{goodsID}")]
        public async Task<IActionResult> checkExist(String GGDNumber, String goodsID)
        {
            var result = await service.checkExist(GGDNumber, goodsID);
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpGet]
        [Route("lastGGDNumber")]
        public async Task<IActionResult> GetLastGGDNumber()
        {
            var GGDNumber = await service.GetLastId();
            if (GGDNumber == null)
            {
                return NotFound();
            }

            return Ok(GGDNumber);
        }
    }
}
