﻿using DevExpress.CodeParser;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class WIDataDetailController : ControllerBase
    {
        private readonly IWIDataDetailService service;

        public WIDataDetailController(IWIDataDetailService service)
        {
            this.service = service;
        }


        [HttpGet]
        [Route("GetByID/{WIDNumber}")]
        public async Task<IActionResult> GetWRDataDetailUnderWIDNumber(String WIDNumber)
        {
            var entrys = await service.GetUnderId(WIDNumber);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WIDataDetailHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByParams3/{WIDNumber}/{palletID}/{partNumber}")]
        public async Task<IActionResult> GetByParams3(String WIDNumber,String palletID,String partNumber)
        {
            var entrys = await service.GetUnderId(WIDNumber,palletID,partNumber);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WIDataDetailHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByParams5/{WIDNumber}/{palletID}/{partNumber}/{lotID}/{*idCode}")]
        public async Task<IActionResult> GetByParams5(String WIDNumber,String palletID, String partNumber,String lotID,String idCode)
        {
            var entrys = await service.GetUnderId(WIDNumber,palletID,partNumber,lotID,idCode);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WIDataDetailHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> Create([FromBody] WIDataDetail wIDataDetail)
        {
            var entry = await service.Create(wIDataDetail);

            return CreatedAtAction(
                 nameof(Get), new { WIDNumber = wIDataDetail.WIDNumber }, entry);
        }

        [HttpPut]
        [Route("Put/{WIDNumber}/{palletID}/{partNumber}/{ordinal}/{lotID}/{idCode}")]
        public async Task<IActionResult> Update(String WIDNumber, String palletID, String partNumber, int ordinal,String lotID,String idCode, [FromBody] WIDataDetail wIDataDetail)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (WIDNumber != wIDataDetail.WIDNumber)
            {
                return BadRequest();
            }

            await service.Update(WIDNumber,palletID, partNumber, ordinal, lotID, idCode,wIDataDetail);


            return new NoContentResult();
        }

        [HttpDelete]
        [Route("Delete/{WIDNumber}")]
        public async Task<IActionResult> Delete(String WIDNumber)
        {
            var entry = await service.Delete(WIDNumber);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }   

        [HttpDelete]
        [Route("DeleteByParams6/{WIDNumber}/{palletId}/{partNumber}/{ordinal}/{lotID}/{idCode}")]
        public async Task<IActionResult> DeleteByParams5(String WIDNumber,String palletID,String partNumber,int ordinal,String lotID,String idCode)
        {
            var entry = await service.Delete(WIDNumber,palletID,partNumber,ordinal,lotID,idCode);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }
    }
}
