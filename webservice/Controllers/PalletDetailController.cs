﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PalletDetailController : Controller
    {
        private readonly IPalletDetailService service;

        public PalletDetailController(IPalletDetailService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Route("GetAll")]
        public async Task<IActionResult> GetAll ()
        {
            var entrys = await service.GetAll();
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = PalletDetailHelper.Covert(entrys);

            return Ok(entryModels);
        }


        [HttpGet]
        [Route("GetByID/{palletID}")]
        public async Task<IActionResult> GetByID(String palletID)
        {
            var entrys = await service.GetUnderId(palletID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = PalletDetailHelper.Covert(entrys);

            return Ok(entryModels);
        }
        [HttpGet]
        [Route("GetByIDCode/{idCode}")]
        public async Task<IActionResult> GetByIDCode(String idCode)
        {
            var entrys = await service.GetUnderIDCode(idCode);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = PalletDetailHelper.Covert(entrys);

            return Ok(entryModels);
        }
        [HttpGet]
        [Route("GetByParams/{palletID}/{idCode}")]
        public async Task<IActionResult> GetByParams(String palletID,String idCode)
        {
            var entrys = await service.GetUnderId(palletID,idCode);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = PalletDetailHelper.Covert(entrys);

            return Ok(entryModels);
        }


        [HttpPost]
        [Route("Post")]
        public IActionResult Create([FromBody] PalletDetail PalletDetail)
        {
            var entry = service.Create(PalletDetail);

            return CreatedAtAction(
                 nameof(GetByID), new { PalletID = PalletDetail.PalletID }, entry);
        }

        [HttpPut]
        [Route("Put/{palletID}/{ordinal}/{idCode}")]
        public IActionResult Update(String palletID, int ordinal,String idCode, [FromBody] PalletDetail PalletDetail)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (palletID != PalletDetail.PalletID)
            {
                return BadRequest();
            }

            service.Update(palletID, ordinal,idCode, PalletDetail);

            return new NoContentResult();
        }

        [HttpDelete]
        [Route("Delete/{palletID}")]
        public IActionResult Delete(String palletID)
        {
            var entry = service.Delete(palletID);

            return Ok(entry);
        }
        [HttpDelete]
        [Route("DeleteByParams/{palletID}/{idCode}")]
        public IActionResult Delete(String palletID,String idCode)
        {
            var entry = service.Delete(palletID,idCode);

            return Ok(entry);
        }



    }
}
