﻿using DevExpress.CodeParser;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class IODetailController : ControllerBase
    {
        private readonly IIODetailService service;

        public IODetailController(IIODetailService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Route("GetByID/{IONumber}")]
        public async Task<IActionResult> GetIODetailUnderIONumber(String IONumber)
        {
            var entrys = await service.GetUnderId(IONumber);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = IODetailHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByParams3/{IONumber}/{palletID}/{partNumber}")]
        public async Task<IActionResult> GetUnderMultiId(String IONumber,String palletID, String partNumber)
        {
            var entrys = await service.GetUnderId(IONumber,palletID, partNumber);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = IODetailHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> Create([FromBody] IODetail IODetail)
        {
            var entry = await service.Create(IODetail);

            return CreatedAtAction(
                 nameof(Get), new { IONumber = IODetail.IONumber }, entry);
        }


        [HttpPut]
        [Route("Put/{IONumber}/{palletID}/{partNumber}/{ordinal}")]
        public async Task<IActionResult> Update(String IONumber,String palletID, String partNumber, int ordinal, [FromBody] IODetail IODetail)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (IONumber != IODetail.IONumber)
            {
                return BadRequest();
            }

            await service.Update(IONumber,palletID, partNumber, ordinal, IODetail);

            return new NoContentResult();
        }

        [HttpDelete]
        [Route("Delete/{IONumber}")]
        public async Task<IActionResult> Delete(String IONumber)
        {
            var entry = await service.Delete(IONumber);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }

        [HttpDelete]
        [Route("DeleteByParams4/{IONumber}/{PalletID}/{PartNumber}/{Ordinal}")]
        public async Task<IActionResult> DeleteByParams4(String IONumber,String palletID,String partNumber,int ordinal)
        {
            var entry = await service.Delete(IONumber,palletID,partNumber,ordinal);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }

    }
}
