﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Interfaces;

namespace webservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class RightController:ControllerBase
    {
        private readonly IRightService rightService;

        public RightController(IRightService rightService)
        {
            this.rightService = rightService;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Get()
        {
            var rights = await rightService.GetAllRight();
            if (rights == null)
            {
                return NotFound();
            }

            var rightModels = RightHelper.CovertRights(rights);

            return Ok(rightModels);
        }
    }
}
