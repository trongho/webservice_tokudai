﻿
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class LabelPrintController:Controller
    {
        private readonly ILabelPrintService service;
        public LabelPrintController(ILabelPrintService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Route("GetByParams6/{WRRNumber}/{PalletID}/{PartNumber}/{LotID}/{IDCode}")]
        public async Task<IActionResult> GetUnderID(string WRRNumber,string PalletID,string PartNumber,string LotID,string idCode)
        {
            var entrys = await service.GetUnderID(WRRNumber,PalletID,PartNumber,LotID,idCode);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = LabelPrintHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByWRRNumber/{WRRNumber}")]
        public async Task<IActionResult> GetUnderWRRNumber(string WRRNumber)
        {
            var entrys = await service.GetUnderID(WRRNumber);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = LabelPrintHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByParams5/{WRRNumber}/{PalletID}/{PartNumber}/{LotID}")]
        public async Task<IActionResult> GetByParams5(string WRRNumber, string PalletID, string PartNumber, string LotID)
        {
            var entrys = await service.GetUnderID(WRRNumber, PalletID, PartNumber, LotID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = LabelPrintHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> Create([FromBody] LabelPrint labelPrint)
        {
            var entry = await service.Create(labelPrint);

            return Ok(entry);

        }

        [HttpPut]
        [Route("Put/{WRRNumber}/{PalletID}/{PartNumber}/{LotID}/{IDCode}")]
        public async Task<IActionResult> Update(string wrrNumber,string palletID,string partNumber,string lotID,string idCode, [FromBody]LabelPrint labelPrint)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (wrrNumber !=labelPrint.WRRNumber)
            {
                return BadRequest();
            }

            await service.Update(labelPrint,wrrNumber,palletID,partNumber,lotID,idCode);

            return new NoContentResult();
        }

        [HttpDelete]
        [Route("Delete/{WRRNumber}/{PalletID}/{PartNumber}/{LotID}")]
        public async Task<IActionResult> Delete(string wrrNumber, string palletID, string partNumber, string lotID)
        {
            var entry = await service.Delete(wrrNumber,palletID,partNumber,lotID);
            return Ok(entry);
        }
    }
}
