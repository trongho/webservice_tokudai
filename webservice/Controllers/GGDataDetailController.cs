﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class GGDataDetailController : ControllerBase
    {
        private readonly IGGDataDetailService service;

        public GGDataDetailController(IGGDataDetailService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Get()
        {
            var entrys = await service.GetAll();
            if (entrys == null)
            {
                return NotFound();
            }
            var entryModels = GGDataDetailHelper.Covert(entrys);
            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByID/{GGDNumber}")]
        public async Task<IActionResult> GetGGDataDetailUnderGGDNumber(String GGDNumber)
        {
            var entrys = await service.GetUnderId(GGDNumber);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = GGDataDetailHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByMultiID/{GGDNumber}/{goodsID}/{ordinal}")]
        public async Task<IActionResult> GetUnderMultiId(String GGDNumber, String goodsID, int ordinal)
        {
            var entrys = await service.GetUnderId(GGDNumber, goodsID, ordinal);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = GGDataDetailHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByIDCode/{GGDNumber}/{goodsID}/{*idCode}")]
        public async Task<IActionResult> GetUnderMultiIdCode(String GGDNumber, String goodsID, String IdCode)
        {
            var entrys = await service.GetUnderIdCode(GGDNumber, goodsID, IdCode);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = GGDataDetailHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> Create([FromBody] GGDataDetail GGDataDetail)
        {
            var entry = await service.Create(GGDataDetail);

            return CreatedAtAction(
                 nameof(Get), new { GGDNumber = GGDataDetail.GGDNumber }, entry);
        }


        [HttpPut]
        [Route("Put/{GGDNumber}/{goodsID}/{ordinal}")]
        public async Task<IActionResult> Update(String GGDNumber, String goodsID, int ordinal, [FromBody] GGDataDetail GGDataDetail)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (GGDNumber != GGDataDetail.GGDNumber)
            {
                return BadRequest();
            }

            await service.Update(GGDNumber, goodsID, ordinal, GGDataDetail);

            return new NoContentResult();
        }

        [HttpDelete]
        [Route("Delete/{GGDNumber}")]
        public async Task<IActionResult> Delete(String GGDNumber)
        {
            var entry = await service.Delete(GGDNumber);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }


        [HttpGet]
        [Route("CheckExist/{GGDNumber}/{goodsID}")]
        public async Task<IActionResult> checkExist(String GGDNumber, String goodsID)
        {
            var result = await service.checkExist(GGDNumber, goodsID);
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpGet]
        [Route("lastGGDNumber")]
        public async Task<IActionResult> GetLastGGDNumber()
        {
            var GGDNumber = await service.GetLastId();
            if (GGDNumber == null)
            {
                return NotFound();
            }

            return Ok(GGDNumber);
        }
    }
}
