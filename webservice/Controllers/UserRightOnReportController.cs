﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UserRightOnReportController:ControllerBase
    {
        private readonly IUserRightOnReportService userRightOnReportService;

        public UserRightOnReportController(IUserRightOnReportService userRightOnReportService)
        {
            this.userRightOnReportService = userRightOnReportService;
        }

        [HttpGet]
        [Route("GetByUserID/{userId}")]
        public async Task<IActionResult> GetUserRightUnderUserId(String userId)
        {
            var entrys = await userRightOnReportService.GetRightByUserId(userId);
            if (entrys == null)
            {
                return NotFound();
            }

            var models = UserRightOnReportHelper.Covert(entrys);

            return Ok(models);
        }

        [HttpGet]
        [Route("GetByReportID/{reportId}")]
        public async Task<IActionResult> GetUserRightUnderReportId(String reportId)
        {
            var entrys = await userRightOnReportService.GetRightByReportId(reportId);
            if (entrys == null)
            {
                return NotFound();
            }

            var models = UserRightOnReportHelper.Covert(entrys);

            return Ok(models);
        }

        [HttpGet]
        [Route("GetByID/{userId}/{reportId}")]
        public async Task<IActionResult> GetUserUnderId(String userId, String reportId)
        {
            var entrys = await userRightOnReportService.GetRightByUserReportId(userId, reportId);
            if (entrys == null)
            {
                return NotFound();
            }

            var models = UserRightOnReportHelper.Covert(entrys);

            return Ok(models);
        }

        [HttpPost]
        [Route("PostUserRightOnReport")]
        public async Task<IActionResult> CreateUserRightOnReport([FromBody] UserRightOnReport userRightOnReport)
        {
            var entrys = await userRightOnReportService.CreateUserRightOnReport(userRightOnReport);

            return CreatedAtAction(
                 nameof(GetUserUnderId), new { userID = userRightOnReport.UserID, reportId = userRightOnReport.ReportID }, userRightOnReport);
        }

        [HttpPut]
        [Route("PutUserRightOnReport/{userId}/{reportId}")]
        public async Task<IActionResult> UpdateUserRightOnReport(String userId, String reportId, [FromBody] UserRightOnReport userRightOnReport)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (userId != userRightOnReport.UserID && reportId != userRightOnReport.ReportID)
            {
                return BadRequest();
            }

            await userRightOnReportService.UpdateUserRightOnReport(userId, reportId, userRightOnReport);

            return new NoContentResult();
        }


        [HttpDelete]
        [Route("DeleteUserRightOnReport/{userId}/{reportId}")]
        public async Task<IActionResult> DeleteUserRightOnReport(String userId, String reportId)
        {
            var entrys = await userRightOnReportService.DeleteUserRightOnReport(userId, reportId);
            if (entrys == null)
            {
                return NotFound();
            }

            return Ok(entrys);
        }

        [HttpGet]
        [Route("CheckExist/{userId}/{reportId}")]
        public async Task<IActionResult> checkExist(String userId, String reportId)
        {
            var result = await userRightOnReportService.checkExist(userId, reportId);
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }
    }
}
