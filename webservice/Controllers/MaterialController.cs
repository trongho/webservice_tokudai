﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class MaterialController : Controller
    {
        private readonly IMaterialService service;

        public MaterialController(IMaterialService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Get()
        {
            var entrys = await service.Get();
            if (entrys == null)
            {
                return NotFound();
            }
            var entryModels = MaterialHelper.Covert(entrys);
            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByID/{id}")]
        public async Task<IActionResult> GetUnderID(String id)
        {
            var entrys = await service.Get(id);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = MaterialHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpPost]
        [Route("Post")]
        public IActionResult Create([FromBody] Material material)
        {
            var entry = service.Create(material);

            return CreatedAtAction(
                 nameof(Get), new { MaterialID = material.MaterialID }, entry);
        }

        [HttpPut]
        [Route("Put/{id}")]
        public IActionResult Update(String id, [FromBody] Material material)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != material.MaterialID)
            {
                return BadRequest();
            }

            service.Update(material, id);

            return new NoContentResult();
        }

        [HttpDelete]
        [Route("Delete/{id}")]
        public IActionResult Delete(String id)
        {
            var entry = service.Delete(id);
            return Ok(entry);
        }

        [HttpDelete]
        [Route("DeleteAll")]
        public IActionResult DeleteAll()
        {
            var entry = service.Delete();
            return Ok(entry);
        }
    }
}
