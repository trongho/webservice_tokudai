﻿using DevExpress.CodeParser;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class WIDataGeneralController : ControllerBase
    {
        private readonly IWIDataGeneralService service;

        public WIDataGeneralController(IWIDataGeneralService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Route("GetByID/{WIDNumber}")]
        public async Task<IActionResult> GetWRDataGeneralUnderWRDNumber(String wIDNumber)
        {
            var entrys = await service.GetUnderId(wIDNumber);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WIDataGeneralHelper.Covert(entrys);

            return Ok(entryModels);
        }



        [HttpGet]
        [Route("GetByParams3/{widNumber}/{palletID}/{partNumber}")]
        public async Task<IActionResult> GetByParams3(String wIDNumber,String palletID,String partNumber)
        {
            var entrys = await service.GetUnderId(wIDNumber,palletID,partNumber);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WIDataGeneralHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> Create([FromBody] WIDataGeneral WIDataGeneral)
        {
            var entry = await service.Create(WIDataGeneral);

            return CreatedAtAction(
                 nameof(Get), new { WRDNumber = WIDataGeneral.WIDNumber }, entry);
        }


        //[HttpPut]
        //[Route("Put/{WIDNumber}/{palletID}/{ordinal}/{lotID}")]
        //public async Task<IActionResult> Update(String wIDNumber, String palletID, int ordinal,String lotID, [FromBody] WIDataGeneral WIDataGeneral)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (wIDNumber != WIDataGeneral.WIDNumber)
        //    {
        //        return BadRequest();
        //    }

        //    await service.Update(wIDNumber, palletID, ordinal,lotID, WIDataGeneral);

        //    return new NoContentResult();
        //}

        [HttpPut]
        [Route("Put/{WIDNumber}/{palletID}/{partNumber}/{ordinal}")]
        public async Task<IActionResult> Update(String wIDNumber, String palletID, int ordinal,String partNumber, [FromBody] WIDataGeneral WIDataGeneral)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (wIDNumber != WIDataGeneral.WIDNumber)
            {
                return BadRequest();
            }

            await service.Update(wIDNumber, palletID,partNumber,ordinal,WIDataGeneral);

            return new NoContentResult();
        }

        [HttpDelete]
        [Route("Delete/{wIDNumber}")]
        public async Task<IActionResult> Delete(String wIDNumber)
        {
            var entry = await service.Delete(wIDNumber);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }

        [HttpDelete]
        [Route("DeleteByParams4/{wIDNumber}/{palletID}/{partNumber}/{ordinal}")]
        public async Task<IActionResult> DeleteByParams4(String wIDNumber,String partNumber,String palletID,int ordinal)
        {
            var entry = await service.Delete(wIDNumber,palletID,partNumber,ordinal);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }
    }
}
