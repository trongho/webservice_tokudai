﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class InventoryOfLastMonthsHelper
    {
        public static List<InventoryOfLastMonthsModel> Covert(List<InventoryOfLastMonths> entrys)
        {
            var models = entrys.ConvertAll(sc => new InventoryOfLastMonthsModel
            {
                Year = sc.Year,
                Month = sc.Month,
                WarehouseID = sc.WarehouseID,
                PartNumber = sc.PartNumber,
                OpeningStockQuantity = sc.OpeningStockQuantity,
                ReceiptQuantity = sc.ReceiptQuantity,
                IssueQuantity = sc.IssueQuantity,
                ClosingStockQuantity = sc.ClosingStockQuantity,
                Status = sc.Status,
                PackingVolume=sc.PackingVolume,
                Material=sc.Material,
                Mold=sc.Mold,
                Unit = sc.Unit,
                IDCode = sc.IDCode,
                PalletID = sc.PalletID
            });

            return models;
        }
    }
}
