﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class TSHeaderHelper
    {
        public static List<TSHeaderModel> Covert(List<TSHeader> entrys)
        {
            var models = entrys.ConvertAll(sc => new TSHeaderModel
            {
               TSNumber=sc.TSNumber,
               TSDate=sc.TSDate,
               WarehouseID=sc.WarehouseID,
               WarehouseName=sc.WarehouseName,
               HandlingStatusID=sc.HandlingStatusID,
               HandlingStatusName=sc.HandlingStatusName,
               Note=sc.Note,
               BranchID=sc.BranchID,
               BranchName=sc.BranchName,
               TotalQuantity=sc.TotalQuantity,
               TotalQuantityByPack=sc.TotalQuantityByPack,
               CheckerID1=sc.CheckerID1,
               CheckerName1=sc.CheckerName1,
               CheckerID2=sc.CheckerID2,
               CheckerName2=sc.CheckerName2,
               Status=sc.Status,
               CreatedUserID=sc.CreatedUserID,
               CreatedDate=sc.CreatedDate,
               UpdatedUserID=sc.UpdatedUserID,
               UpdatedDate=sc.UpdatedDate,
            });

            return models;
        }
    }
}
