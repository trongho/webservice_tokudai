﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class IODetailHelper
    {
        public static List<IODetailModel> Covert(List<IODetail> entrys)
        {
            var models = entrys.ConvertAll(sc => new IODetailModel
            {
                IONumber = sc.IONumber,
                Ordinal = sc.Ordinal,
                PalletID = sc.PalletID,
                IDCode = sc.IDCode,
                PartNumber = sc.PartNumber,
                LotID=sc.LotID,
                PartName = sc.PartName,
                Unit = sc.Unit,
                QuantityOrdered = sc.QuantityOrdered,
                IssueQuantity = sc.IssueQuantity,
                PackingQuantity=sc.PackingQuantity,
                PackingVolume=sc.PackingVolume,
                Status = sc.Status,
                MFGDate = sc.MFGDate,
                EXPDate = sc.EXPDate,
                InputDate=sc.InputDate,
                OutputDate=sc.OutputDate
            });

            return models;
        }
    }
}
