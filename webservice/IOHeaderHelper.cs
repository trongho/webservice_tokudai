﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class IOHeaderHelper
    {
        public static List<IOHeaderModel> Covert(List<IOHeader> entrys)
        {
            var models = entrys.ConvertAll(sc => new IOHeaderModel
            {
                IONumber = sc.IONumber,
                ModalityID = sc.ModalityID,
                ModalityName = sc.ModalityName,
                IODate = sc.IODate,
                WIRNumber = sc.WIRNumber,
                WarehouseID = sc.WarehouseID,
                WarehouseName = sc.WarehouseName,
                HandlingStatusID = sc.HandlingStatusID,
                HandlingStatusName = sc.HandlingStatusName,
                BranchID = sc.BranchID,
                BranchName = sc.BranchName,
                TotalQuantity = sc.TotalQuantity,
                Note = sc.Note,
                Status = sc.Status,
                CreatedUserID = sc.CreatedUserID,
                CreatedDate = sc.CreatedDate,
                UpdatedUserID = sc.UpdatedUserID,
                UpdatedDate = sc.UpdatedDate,
            });

            return models;
        }
    }
}
