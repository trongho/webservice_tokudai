﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class LabelPrintHelper
    {
        public static List<LabelPrintModel> Covert(List<LabelPrint> entrys)
        {
            var models = entrys.ConvertAll(sc => new LabelPrintModel
            {
                WRRNumber = sc.WRRNumber,
                Ordinal = sc.Ordinal,
                PalletID = sc.PalletID,
                PartNumber = sc.PartNumber,
                PartName = sc.PartName,
                ProductFamily = sc.ProductFamily,
                LotID = sc.LotID,
                Quantity= sc.Quantity,
                Unit = sc.Unit,
                MFGDate = sc.MFGDate,
                EXPDate = sc.EXPDate,
                IDCode = sc.IDCode,
                Status=sc.Status,
            });

            return models;
        }
    }
}
