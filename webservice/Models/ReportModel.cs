﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webservice.Models
{
    public class ReportModel
    {
        public String ReportID { get; set; }
        public String? ReportName { get; set; }
        public String? ReportNameEN { get; set; }
        public Int16? Ordinal { get; set; }
    }
}
