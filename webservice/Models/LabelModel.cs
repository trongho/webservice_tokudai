﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webservice.Models
{
    public class LabelModel
    {
        public string PalletID { get; set; }
        public int? CurrentIDCode { get; set; }
    }
}
