﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webservice.Models
{
    public class UserRightModel
    {
        public String UserID { get; set; }
        public Int16 Ordinal { get; set; }
        public String RightID { get; set; }
        public String RightName { get; set; }
        public String RightNameEN { get; set; }
        public Int16 GrantRight { get; set; }
    }
}
