﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webservice.Models
{
    public class GoodsModel
    {
        public string PartNumber { get; set; }
        public string PalletID { get; set; }
        public int Ordinal { get; set; }
        public string PartName { get; set; }
        public string Unit { get; set; }
        public string ProductFamily { get; set; }
        public decimal? Quantity { get; set; }
        public string Mold { get; set; }
        public string CartonName { get; set; }
        public string CartonSize { get; set; }
        public string Pads { get; set; }
        public string Nilon { get; set; }
        public string LabelSize { get; set; }
        public string Material { get; set; }
        public string Status { get; set; }
        public string CreatedUserID { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdatedUserID { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? CurrentIDCode { get; set; }
    }
}
