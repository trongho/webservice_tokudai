﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webservice.Models
{
    public class WRDetailModel
    {
        public string WRNumber { get; set; }
        public int Ordinal { get; set; }
        public string PalletID { get; set; }
        public string IDCode { get; set; }
        public string PartNumber { get; set; }
        public string LotID { get; set; }
        public string PartName { get; set; }
        public string Unit { get; set; }
        public decimal? QuantityReceived { get; set; }
        public decimal? ReceiptQuantity { get; set; }
        public decimal? PackingVolume { get; set; }
        public decimal? PackingQuantity { get; set; }
        public string Status { get; set; }
        public DateTime? MFGDate { get; set; }
        public DateTime? EXPDate { get; set; }
        public DateTime? InputDate { get; set; }
    }
}
