﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webservice.Models
{
    public class WIDataDetailModel
    {
        public String WIDNumber { get; set; }
        public int Ordinal { get; set; }
        public String PalletID { get; set; }
        public String PartNumber { get; set; }
        public String PartName { get; set; }
        public String ProductFamily { get; set; }
        public String LotID { get; set; }
        public String IDCode { get; set; }
        public decimal Quantity { get; set; }
        public DateTime MFGDate { get; set; }
        public DateTime EXPDate { get; set; }
        public String CreatorID { get; set; }
        public String CreatedDateTime { get; set; }
        public String EditerID { get; set; }
        public String EditedDateTime { get; set; }
        public String Status { get; set; }
    }
}
