﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webservice.Models
{
    public class MaterialModel
    {
        public string MaterialID { get; set; }
        public string MaterialName { get; set; }
    }
}
