﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class WIDataDetailHelper
    {
        public static List<WIDataDetailModel> Covert(List<WIDataDetail> entrys)
        {
            var models = entrys.ConvertAll(sc => new WIDataDetailModel
            {
                WIDNumber = sc.WIDNumber,
                Ordinal = sc.Ordinal,
                PalletID=sc.PalletID,
                PartNumber=sc.PartNumber,
                PartName=sc.PartName,
                ProductFamily = sc.ProductFamily,
                LotID=sc.LotID,
                IDCode = sc.IDCode,
                Quantity = sc.Quantity,
                MFGDate=sc.MFGDate,
                EXPDate=sc.EXPDate,
                CreatorID = sc.CreatorID,
                CreatedDateTime = sc.CreatedDateTime,
                EditerID = sc.EditerID,
                EditedDateTime = sc.EditedDateTime,
                Status = sc.Status,
            });

            return models;
        }
    }
}
