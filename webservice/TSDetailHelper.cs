﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class TSDetailHelper
    {
        public static List<TSDetailModel> Covert(List<TSDetail> entrys)
        {
            var models = entrys.ConvertAll(sc => new TSDetailModel
            {
               TSNumber=sc.TSNumber,
               Ordinal=sc.Ordinal,
               PartNumber=sc.PartNumber,
               PartName=sc.PartName,
               TallyUnitID=sc.TallyUnitID,
               UnitRate=sc.UnitRate,
               StockUnitID=sc.StockUnitID,
               PackingVolume=sc.PackingVolume,
               Quantity=sc.Quantity,
                QuantityByPack=sc.QuantityByPack,
               Note=sc.Note,
               Status=sc.Status,
            });

            return models;
        }
    }
}
