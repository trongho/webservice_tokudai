﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class WarehouseHelper
    {
        public static List<WarehouseModel> Covert(List<Warehouse> entrys)
        {
            var models = entrys.ConvertAll(sc => new WarehouseModel
            {
                WarehouseID = sc.WarehouseID,
                WarehouseName = sc.WarehouseName,
                Address = sc.Address,
                ProvinceID = sc.ProvinceID,
                DistrictID = sc.DistrictID,
                Representative = sc.Representative,
                Position = sc.Position,
                TelNumber = sc.TelNumber,
                FaxNumber = sc.FaxNumber,
                Email = sc.Email,
                BranchID = sc.BranchID,
                Description = sc.Description,
                Status = sc.Status,
                CreatedUserID = sc.CreatedUserID,
                CreatedDate = sc.CreatedDate,
                UpdatedUserID = sc.UpdatedUserID,
                UpdatedDate = sc.UpdatedDate,
            });

            return models;
        }
    }
}
