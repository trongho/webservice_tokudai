﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class InventoryHelper
    {
        public static List<InventoryModel> Covert(List<Inventory> entrys)
        {
            var models = entrys.ConvertAll(sc => new InventoryModel
            {
                Year=sc.Year,
                Month=sc.Month,
                WarehouseID=sc.WarehouseID,
                PartNumber=sc.PartNumber,
                OpeningStockQuantity=sc.OpeningStockQuantity,
                ReceiptQuantity=sc.ReceiptQuantity,
                IssueQuantity=sc.IssueQuantity,
                ClosingStockQuantity=sc.ClosingStockQuantity,
                Status=sc.Status,
                PackingVolume=sc.PackingVolume,
                Material=sc.Material,
                Mold=sc.Mold,
                Unit=sc.Unit,
                IDCode=sc.IDCode,
                PalletID=sc.PalletID
                
            });

            return models;
        }
    }
}
