﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class LabelHelper
    {
        public static List<LabelModel> Covert(List<Label> entrys)
        {
            var models = entrys.ConvertAll(sc => new LabelModel
            {
               PalletID=sc.PalletID,
               CurrentIDCode=sc.CurrentIDCode,
            });

            return models;
        }
    }
}
