﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class GoodsHelper
    {
        public static List<GoodsModel> Covert(List<Goods> entrys)
        {
            var models = entrys.ConvertAll(sc => new GoodsModel
            {
                PartNumber = sc.PartNumber,
                Ordinal=sc.Ordinal,
                PartName = sc.PartName,
                Unit = sc.Unit,
                ProductFamily = sc.ProductFamily,
                Quantity = sc.Quantity,
                PalletID = sc.PalletID,
                Mold = sc.Mold,
                CartonName = sc.CartonName,
                CartonSize = sc.CartonSize,
                Pads = sc.Pads,
                Nilon = sc.Nilon,
                LabelSize = sc.LabelSize,
                Material = sc.Material,
                Status = sc.Status,
                CreatedUserID = sc.CreatedUserID,
                CreatedDate = sc.CreatedDate,
                UpdatedUserID = sc.UpdatedUserID,
                UpdatedDate = sc.UpdatedDate,
                CurrentIDCode=sc.CurrentIDCode
            });

            return models;
        }
    }
}
