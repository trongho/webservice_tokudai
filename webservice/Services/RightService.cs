﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class RightService:IRightService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public RightService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }

        public async Task<List<Right>> GetAllRight()
        {
            var rights = warehouseDbContext.Rights;
            return await rights.ToListAsync();
        }
    }
}
