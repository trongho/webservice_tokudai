﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class InventoryOfLastMonthsService: IInventoryOfLastMonthsService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public InventoryOfLastMonthsService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }

        public async Task<bool> checkExist(short year, short month, string warehouseID)
        {
            Boolean flag = false;
            var entrys = warehouseDbContext.InventoryOfLastMonthss.FirstOrDefault(g => g.Year == year
            && g.Month == month && g.WarehouseID.Equals(warehouseID));
            if (entrys != null)
            {
                flag = true;
            }
            return flag;
        }

        public async Task<bool> checkExist(short year, short month, string warehouseID, string PartNumber)
        {
            Boolean flag = false;
            var entrys = warehouseDbContext.InventoryOfLastMonthss.FirstOrDefault(g => g.Year == year
            && g.Month == month && g.WarehouseID.Equals(warehouseID) && g.PartNumber.Equals(PartNumber));
            if (entrys != null)
            {
                flag = true;
            }
            return flag;
        }

        public async Task<bool> Create(InventoryOfLastMonths InventoryOfLastMonths)
        {
            var entrys = warehouseDbContext.InventoryOfLastMonthss.AddAsync(InventoryOfLastMonths);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(short year, short month, string warehouseID, string PartNumber)
        {
            var entrys = warehouseDbContext.InventoryOfLastMonthss.Where(g => g.Year == year
            && g.Month == month && g.WarehouseID.Equals(warehouseID) && g.PartNumber.Equals(PartNumber));
            warehouseDbContext.InventoryOfLastMonthss.RemoveRange(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<bool> Delete(short year, short month, string warehouseID)
        {
            var entrys = warehouseDbContext.InventoryOfLastMonthss.Where(g => g.Year == year
            && g.Month == month && g.WarehouseID.Equals(warehouseID));
            warehouseDbContext.InventoryOfLastMonthss.RemoveRange(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<bool> Delete()
        {
            var entrys = warehouseDbContext.InventoryOfLastMonthss;
            warehouseDbContext.InventoryOfLastMonthss.RemoveRange(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<bool> DeleteOldDate(string warehouseID)
        {
            var entrys = warehouseDbContext.InventoryOfLastMonthss.Where(g => ((g.Year == System.DateTime.Now.Year&&g.Month < System.DateTime.Now.Month-1)|| g.Year < System.DateTime.Now.Year)
            && g.WarehouseID.Equals(warehouseID));
            warehouseDbContext.InventoryOfLastMonthss.RemoveRange(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<InventoryOfLastMonths>> GetAll()
        {
            var entrys = warehouseDbContext.InventoryOfLastMonthss;
            return await entrys.ToListAsync();
        }

        public async Task<List<InventoryOfLastMonths>> GetUnderId(short year, short month, string warehouseID)
        {
            var entrys = warehouseDbContext.InventoryOfLastMonthss.Where(g => g.Year == year
            && g.Month == month && g.WarehouseID.Equals(warehouseID));
            return await entrys.ToListAsync();
        }

        public async Task<List<InventoryOfLastMonths>> GetUnderId(short year, short month, string warehouseID, string PartNumber)
        {
            var entrys = warehouseDbContext.InventoryOfLastMonthss.Where(g => g.Year == year
            && g.Month == month && g.WarehouseID.Equals(warehouseID) && g.PartNumber.Equals(PartNumber));
            return await entrys.ToListAsync();
        }

        public async Task<List<InventoryOfLastMonths>> GetUnderId(string warehouseID, string partNumber)
        {
            var entrys = warehouseDbContext.InventoryOfLastMonthss.Where(g => g.WarehouseID.Equals(warehouseID) && g.PartNumber.Equals(partNumber));
            return await entrys.ToListAsync();
        }

        public async Task<bool> Update(short year, short month, string warehouseID, string PartNumber, InventoryOfLastMonths InventoryOfLastMonths)
        {
            var entrys = warehouseDbContext.InventoryOfLastMonthss.FirstOrDefault(g => g.Year == year
            && g.Month == month && g.WarehouseID.Equals(warehouseID) && g.PartNumber.Equals(PartNumber));
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(InventoryOfLastMonths);
            warehouseDbContext.SaveChanges();
            return true;
        }
    }
}
