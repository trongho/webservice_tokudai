﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class BranchService:IBranchService
    {
        private readonly WarehouseDbContext warehouseDbContext;
        public BranchService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }

        public async Task<bool> checkExist(string id)
        {
            Boolean flag = false;
            var entry = warehouseDbContext.Branchs.FirstOrDefault(g => g.BranchID.Equals(id));
            if (entry != null)
            {
                flag = true;
            }
            return flag;
        }

        public async Task<bool> Create(Branch branch)
        {
            var entry = warehouseDbContext.Branchs.AddAsync(branch);
            warehouseDbContext.SaveChanges();

            return entry.IsCompleted;
        }

        public async Task<bool> Delete(string id)
        {
            var entry = warehouseDbContext.Branchs.FirstOrDefault(o => o.BranchID.Equals(id));
            warehouseDbContext.Branchs.Remove(entry);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<Branch>> GetAll()
        {
            var entrys = warehouseDbContext.Branchs;
            return await entrys.ToListAsync();
        }

        public async Task<List<Branch>> GetUnderId(string id)
        {
            var entry = warehouseDbContext.Branchs.Where(u=>u.BranchID.Equals(id));
            return await entry.ToListAsync();
        }

        public async Task<bool> Update(string id, Branch branch)
        {
            var entry = warehouseDbContext.Branchs.FirstOrDefault(o => o.BranchID.Equals(id));
            warehouseDbContext.Entry(entry).CurrentValues.SetValues(branch);
            warehouseDbContext.SaveChanges();
            return true;
        }
    }
}
