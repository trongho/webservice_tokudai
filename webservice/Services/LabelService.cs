﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class LabelService: ILabelService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public LabelService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }

        public async Task<bool> Create(Label Label)
        {
            var entrys = warehouseDbContext.Labels.AddAsync(Label);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string palletID)
        {
            var entrys = warehouseDbContext.Labels.FirstOrDefault(o =>o.PalletID.Equals(palletID));
            warehouseDbContext.Labels.Remove(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<bool> Delete()
        {
            var entrys = warehouseDbContext.Labels;
            warehouseDbContext.Labels.RemoveRange(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<Label>> GetAll()
        {
            var entrys = warehouseDbContext.Labels;
            return await entrys.ToListAsync();
        }

        public async Task<List<Label>> GetUnderId(string palletID)
        {
            var entrys = warehouseDbContext.Labels.Where(u =>u.PalletID.Equals(palletID));
            return await entrys.ToListAsync();
        }

        public async Task<bool> Update(string palletID, Label Label)
        {
            var entrys = warehouseDbContext.Labels.FirstOrDefault(o =>o.PalletID.Equals(palletID));
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(Label);
            warehouseDbContext.SaveChanges();
            return true;
        }
    }
}
