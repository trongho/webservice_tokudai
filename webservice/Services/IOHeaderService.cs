﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class IOHeaderService : IIOHeaderService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public  IOHeaderService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }

        public async Task<bool> checkExist(string id)
        {
            Boolean flag = false;
            var entrys = warehouseDbContext.IOHeaders.FirstOrDefault(g => g.IONumber.Equals(id));
            if (entrys != null)
            {
                flag = true;
            }
            return flag;
        }

        public async Task<bool> Create(IOHeader IOHeader)
        {
            var entrys = warehouseDbContext.IOHeaders.AddAsync(IOHeader);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string id)
        {
            var entrys = warehouseDbContext.IOHeaders.FirstOrDefault(o => o.IONumber.Equals(id));
            warehouseDbContext.IOHeaders.Remove(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<IOHeader>> GetAll()
        {
            var entrys = warehouseDbContext.IOHeaders;
            return await entrys.ToListAsync();
        }

        public async Task<string> GetLastId()
        {
            var curentID = warehouseDbContext.IOHeaders.OrderByDescending(x => x.IONumber).Take(1).Select(x => x.IONumber).ToList().FirstOrDefault();
            return curentID;
        }

        public async Task<List<IOHeader>> GetUnderBranchID(string branchID)
        {
            var entrys = warehouseDbContext.IOHeaders.Where(u => u.BranchID.Equals(branchID));
            return await entrys.ToListAsync();
        }

        public async Task<List<IOHeader>> GetUnderDate(DateTime fromDate, DateTime toDate)
        {
            var entrys = warehouseDbContext.IOHeaders.Where(u => u.IODate >= fromDate && u.IODate <= toDate);
            return await entrys.ToListAsync();
        }

        public async Task<List<IOHeader>> GetUnderHandlingStatusID(string handlingStatusID)
        {
            var entrys = warehouseDbContext.IOHeaders.Where(u => u.HandlingStatusID.Equals(handlingStatusID));
            return await entrys.ToListAsync();
        }

        public async Task<List<IOHeader>> GetUnderModalityID(string modalityID)
        {
            var entrys = warehouseDbContext.IOHeaders.Where(u => u.ModalityID.Equals(modalityID));
            return await entrys.ToListAsync();
        }

        public async Task<List<IOHeader>> GetUnderId(string id)
        {
            var entrys = warehouseDbContext.IOHeaders.Where(u => u.IONumber.Equals(id));
            return await entrys.ToListAsync();
        }

       

        public async Task<List<IOHeader>> GetUnderMonthYear(string monthYear)
        {
            String year = monthYear.Substring(3, 4);
            String month = monthYear.Substring(0, 2);
            var entrys = warehouseDbContext.IOHeaders.Where(u => u.IODate.Value.Year ==int.Parse(year) && u.IODate.Value.Month ==int.Parse(month));
            return await entrys.ToListAsync();
        }

        public async Task<bool> Update(string id, IOHeader IOHeader)
        {
            var entrys = warehouseDbContext.IOHeaders.FirstOrDefault(o => o.IONumber.Equals(id));
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(IOHeader);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<IOHeader>> GetUnderLastMonthYear(string monthYear)
        {
            String year = monthYear.Substring(3, 4);
            String month = monthYear.Substring(0, 2);
            int yearInt = int.Parse(year);
            int monthInt = int.Parse(month);
            int mLastMonth;
            int mLastYear;
            if (monthInt == 1)
            {
                mLastMonth = 12;
                mLastYear = yearInt - 1;
            }
            else
            {
                mLastMonth = monthInt - 1;
                mLastYear = yearInt;
            }
            if (monthInt == 1)
            {
                mLastMonth = 12;
                mLastYear = yearInt - 1;
            }
            else
            {
                mLastMonth = monthInt - 1;
                mLastYear = yearInt;
            }
            var entrys = warehouseDbContext.IOHeaders.Where(u => u.IODate.Value.Year != mLastYear || u.IODate.Value.Month <= monthInt);
            return await entrys.ToListAsync();
        }
    }
}
