﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class OrderStatusService : IOrderStatusService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public OrderStatusService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }

        public async Task<List<OrderStatus>> GetAll()
        {
            var entrys = warehouseDbContext.OrderStatuss;
            return await entrys.ToListAsync();
        }
    }
}
