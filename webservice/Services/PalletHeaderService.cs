﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class PalletHeaderService : IPalletHeaderService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public PalletHeaderService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }
        public bool Create(PalletHeader PalletHeader)
        {
            var entrys = warehouseDbContext.PalletHeaders.AddAsync(PalletHeader);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public bool Delete(string id)
        {
            var entrys = warehouseDbContext.PalletHeaders.FirstOrDefault(o => o.PalletID.Equals(id));
            warehouseDbContext.PalletHeaders.Remove(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<PalletHeader>> GetAll()
        {
            var entrys = warehouseDbContext.PalletHeaders;
            return await entrys.ToListAsync();
        }

        public string GetLastId(string S)
        {
            var lastID = warehouseDbContext.PalletHeaders.Where(u => u.PalletID.Contains(S)).OrderByDescending(x => x.PalletID).Take(1).Select(x => x.PalletID).ToList().FirstOrDefault();
            return lastID;
        }

        public async Task<List<PalletHeader>> GetUnderCreatedDate(DateTime fromDate,DateTime toDate)
        {
            var entrys = warehouseDbContext.PalletHeaders.Where(u => u.CreatedDate>=fromDate&&u.CreatedDate<=toDate);
            return await entrys.ToListAsync();
        }

        public async Task<List<PalletHeader>> GetUnderId(string id)
        {
            var entrys = warehouseDbContext.PalletHeaders.Where(u => u.PalletID.Equals(id));
            return await entrys.ToListAsync();
        }

        public async Task<List<PalletHeader>> GetUnderStatus(string status)
        {
            var entrys = warehouseDbContext.PalletHeaders.Where(u => u.Status.Equals(status));
            return await entrys.ToListAsync();
        }

        public bool Update(string id,int ordinal, PalletHeader PalletHeader)
        {
            var entrys = warehouseDbContext.PalletHeaders.FirstOrDefault(o => o.PalletID.Equals(id)&&o.Ordinal==ordinal);
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(PalletHeader);
            warehouseDbContext.SaveChanges();
            return true;
        }
    }
}
