﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class WRRDetailService:IWRRDetailService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public WRRDetailService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }

        public async Task<bool> Create(WRRDetail wRRDetail)
        {
            var entrys = warehouseDbContext.WRRDetails.AddAsync(wRRDetail);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string id)
        {
            var entrys = warehouseDbContext.WRRDetails.Where(o => o.WRRNumber.Equals(id));
            warehouseDbContext.WRRDetails.RemoveRange(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<bool> Delete(string wRRNumber, string palletID, int Ordinal)
        {
            var entrys = warehouseDbContext.WRRDetails.Where(o => o.WRRNumber.Equals(wRRNumber)&&o.PalletID.Equals(palletID)&&o.Ordinal==Ordinal).FirstOrDefault();
            warehouseDbContext.WRRDetails.Remove(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<WRRDetail>> GetAll()
        {
            var entrys = warehouseDbContext.WRRDetails;
            return await entrys.ToListAsync();
        }

        public async Task<List<WRRDetail>> GetUnderId(string wRRNumber, string palletID, int Ordinal)
        {
            var entrys = warehouseDbContext.WRRDetails.Where(u => u.WRRNumber.Equals(wRRNumber)&&u.PalletID.Equals(palletID)&&u.Ordinal==Ordinal);
            return await entrys.ToListAsync();
        }

        public async Task<List<WRRDetail>> GetUnderId(string wRRNumber)
        {
            var entrys = warehouseDbContext.WRRDetails.Where(u => u.WRRNumber.Equals(wRRNumber));
            return await entrys.ToListAsync();
        }

        public async Task<List<WRRDetail>> GetUnderId(string wRRNumber, string palletID)
        {
            var entrys = warehouseDbContext.WRRDetails.Where(u => u.WRRNumber.Equals(wRRNumber) && u.PalletID.Equals(palletID));
            return await entrys.ToListAsync();
        }

        public async Task<List<WRRDetail>> GetUnderId(string wRRNumber, string palletID, string lotID)
        {
            var entrys = warehouseDbContext.WRRDetails.Where(u => u.WRRNumber.Equals(wRRNumber) && u.PalletID.Equals(palletID)&&u.LotID.Equals(lotID));
            return await entrys.ToListAsync();
        }

        public async Task<bool> Update(string wRRNumber, String palletID, int Ordinal,String lotID, WRRDetail wRRDetail)
        {
            var entrys = warehouseDbContext.WRRDetails.FirstOrDefault(o => o.WRRNumber.Equals(wRRNumber) && o.PalletID.Equals(palletID) && o.Ordinal==Ordinal&&o.LotID.Equals(lotID));
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(wRRDetail);
            warehouseDbContext.SaveChanges();
            return true;
        }
    }
}
