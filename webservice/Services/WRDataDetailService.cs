﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class WRDataDetailService : IWRDataDetailService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public WRDataDetailService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }
        public async Task<List<WRDataDetail>> GetUnderId(string wRDNumber, string palletID)
        {
            var entrys = warehouseDbContext.WRDataDetails.Where(u => u.WRDNumber.Equals(wRDNumber) && u.PalletID.Equals(palletID));
            return await entrys.ToListAsync();
        }

        public async Task<List<WRDataDetail>> GetUnderId(string wRDNumber)
        {
            var entrys = warehouseDbContext.WRDataDetails.Where(u => u.WRDNumber.Equals(wRDNumber));
            return await entrys.ToListAsync();
        }
    }
}
