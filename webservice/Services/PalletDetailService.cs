﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class PalletDetailService : IPalletDetailService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public PalletDetailService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }
        public bool Create(PalletDetail PalletDetail)
        {
            var entrys = warehouseDbContext.PalletDetails.AddAsync(PalletDetail);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public bool Delete(string palletID)
        {
            var entrys = warehouseDbContext.PalletDetails.Where(o => o.PalletID.Equals(palletID));
            warehouseDbContext.PalletDetails.RemoveRange(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public bool Delete(string palletID, string idCode)
        {
            var entrys = warehouseDbContext.PalletDetails.Where(o => o.PalletID.Equals(palletID)&&o.IDCode.Equals(idCode)).FirstOrDefault();
            warehouseDbContext.PalletDetails.Remove(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<PalletDetail>> GetAll()
        {
            var entrys = warehouseDbContext.PalletDetails;
            return await entrys.ToListAsync();
        }

        public async Task<List<PalletDetail>> GetUnderId(string palletID)
        {
            var entrys = warehouseDbContext.PalletDetails.Where(u => u.PalletID.Equals(palletID));
            return await entrys.ToListAsync();
        }

        public async Task<List<PalletDetail>> GetUnderId(string palletID, string idCode)
        {
            var entrys = warehouseDbContext.PalletDetails.Where(u => u.PalletID.Equals(palletID)&&u.IDCode.Equals(idCode));
            return await entrys.ToListAsync();
        }

        public async Task<List<PalletDetail>> GetUnderIDCode(string idCode)
        {
            var entrys = warehouseDbContext.PalletDetails.Where(u => u.IDCode.Equals(idCode));
            return await entrys.ToListAsync();
        }

        public bool Update(string palletID,int ordinal, string idCode, PalletDetail PalletDetail)
        {
            var entrys = warehouseDbContext.PalletDetails.FirstOrDefault(o => o.PalletID.Equals(palletID) && o.Ordinal ==ordinal && o.IDCode.Equals(idCode));
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(PalletDetail);
            warehouseDbContext.SaveChanges();
            return true;
        }
    }
}
