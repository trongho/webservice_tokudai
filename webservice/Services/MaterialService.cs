﻿using System;
using System.Collections.Generic;
using System.Linq;
using webservice.Entities;

using System.Threading.Tasks;
using webservice.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace webservice.Services
{
    public class MaterialService : IMaterialService
    {
        private readonly WarehouseDbContext context;

        public MaterialService(WarehouseDbContext context)
        {
            this.context = context;
        }
        public bool Create(Material entry)
        {
            var entrys = context.Materials.AddAsync(entry);
            context.SaveChanges();

            return entrys.IsCompleted;
        }

        public bool Delete()
        {
            var entrys = context.Materials;
            context.Materials.RemoveRange(entrys);
            context.SaveChanges();
            return true;
        }

        public bool Delete(string id)
        {
            var entrys = context.Materials.FirstOrDefault(o => o.MaterialID.Equals(id));
            context.Materials.Remove(entrys);
            context.SaveChanges();
            return true;
        }

        public async Task<List<Material>> Get()
        {
            var entrys = context.Materials;
            return await entrys.ToListAsync();
        }

        public async Task<List<Material>> Get(string id)
        {
            var entrys = context.Materials.Where(u => u.MaterialName.Equals(id));
            return await entrys.ToListAsync();
        }

        public bool Update(Material material, string id)
        {
            var entrys =context.Materials.FirstOrDefault(o => o.MaterialID.Equals(id));
            context.Entry(entrys).CurrentValues.SetValues(material);
            context.SaveChanges();
            return true;
        }
    }
}
