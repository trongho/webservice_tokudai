﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class InventoryOfLastMonthDetailService: IInventoryOfLastMonthDetailService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public InventoryOfLastMonthDetailService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }
        public async Task<bool> Create(InventoryOfLastMonthDetail InventoryOfLastMonthDetail)
        {
            var entrys = warehouseDbContext.InventoryOfLastMonthDetails.AddAsync(InventoryOfLastMonthDetail);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(short year, short month, string warehouseID, string partNumber, string palletID)
        {
            var entrys = warehouseDbContext.InventoryOfLastMonthDetails.Where(g => g.Year == year
            && g.Month == month && g.WarehouseID.Equals(warehouseID) && g.PartNumber.Equals(partNumber) && g.PalletID.Equals(palletID)).FirstOrDefault();
            warehouseDbContext.InventoryOfLastMonthDetails.Remove(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<bool> Delete(short year, short month, string warehouseID, string partNumber)
        {
            var entrys = warehouseDbContext.InventoryOfLastMonthDetails.Where(g => g.Year == year
            && g.Month == month && g.WarehouseID.Equals(warehouseID) && g.PartNumber.Equals(partNumber));
            warehouseDbContext.InventoryOfLastMonthDetails.RemoveRange(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<bool> Delete(short year, short month, string warehouseID)
        {
            var entrys = warehouseDbContext.InventoryOfLastMonthDetails.Where(g => g.Year == year
            && g.Month == month && g.WarehouseID.Equals(warehouseID));
            warehouseDbContext.InventoryOfLastMonthDetails.RemoveRange(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<bool> Delete(short year, short month, string warehouseID, string partNumber, string palletID, string idCode)
        {
            var entrys = warehouseDbContext.InventoryOfLastMonthDetails.Where(g => g.Year == year
            && g.Month == month && g.WarehouseID.Equals(warehouseID) && g.PartNumber.Equals(partNumber) && g.PalletID.Equals(palletID) && g.IDCode.Equals(idCode)).FirstOrDefault();
            warehouseDbContext.InventoryOfLastMonthDetails.Remove(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<bool> DeleteOldDate(string warehouseID)
        {
            var entrys = warehouseDbContext.InventoryOfLastMonthDetails.Where(g => ((g.Year == System.DateTime.Now.Year && g.Month < System.DateTime.Now.Month - 1) || g.Year < System.DateTime.Now.Year)
            && g.WarehouseID.Equals(warehouseID));
            warehouseDbContext.InventoryOfLastMonthDetails.RemoveRange(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<InventoryOfLastMonthDetail>> GetAll()
        {
            var entrys = warehouseDbContext.InventoryOfLastMonthDetails;
            return await entrys.ToListAsync();
        }

        public async Task<List<InventoryOfLastMonthDetail>> GetUnderId(short year, short month, string warehouseID, string partNumber)
        {
            var entrys = warehouseDbContext.InventoryOfLastMonthDetails.Where(g => g.Year == year
            && g.Month == month && g.WarehouseID.Equals(warehouseID) && g.PartNumber.Equals(partNumber));
            return await entrys.ToListAsync();
        }

        public async Task<List<InventoryOfLastMonthDetail>> GetUnderId(short year, short month, string warehouseID, string partNumber, string palletID)
        {
            var entrys = warehouseDbContext.InventoryOfLastMonthDetails.Where(g => g.Year == year
            && g.Month == month && g.WarehouseID.Equals(warehouseID) && g.PartNumber.Equals(partNumber) && g.PalletID.Equals(palletID));
            return await entrys.ToListAsync();
        }

        public async Task<List<InventoryOfLastMonthDetail>> GetUnderId2(short year, short month, string warehouseID, string partNumber, string palletID, string idCode)
        {
            var entrys = warehouseDbContext.InventoryOfLastMonthDetails.Where(g => g.Year == year
            && g.Month == month && g.WarehouseID.Equals(warehouseID) && g.PartNumber.Equals(partNumber) && g.PalletID.Equals(palletID) && g.IDCode.Equals(idCode));
            return await entrys.ToListAsync();
        }

        public async Task<List<InventoryOfLastMonthDetail>> GetUnderId(short year, short month, string warehouseID, string partNumber, string palletID, string lotID)
        {
            var entrys = warehouseDbContext.InventoryOfLastMonthDetails.Where(g => g.Year == year
           && g.Month == month && g.WarehouseID.Equals(warehouseID) && g.PartNumber.Equals(partNumber) && g.PalletID.Equals(palletID) && g.LotID.Equals(lotID));
            return await entrys.ToListAsync();
        }

        public async Task<bool> Update(short year, short month, string warehouseID, string partNumber, string palletID, InventoryOfLastMonthDetail InventoryOfLastMonthDetail)
        {
            var entrys = warehouseDbContext.InventoryOfLastMonthDetails.FirstOrDefault(g => g.Year == year
            && g.Month == month && g.WarehouseID.Equals(warehouseID) && g.PartNumber.Equals(partNumber) && g.PalletID.Equals(palletID));
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(InventoryOfLastMonthDetail);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<InventoryOfLastMonthDetail>> GetUnderId(short year, short month, string warehouseID)
        {
            var entrys = warehouseDbContext.InventoryOfLastMonthDetails.Where(g => g.Year == year
           && g.Month == month && g.WarehouseID.Equals(warehouseID));
            return await entrys.ToListAsync();
        }

        public async Task<List<InventoryOfLastMonthDetail>> GetUnderId(string warehouseID, string partNumber, string palletID, string idCode)
        {
            var entrys = warehouseDbContext.InventoryOfLastMonthDetails.Where(g => g.WarehouseID.Equals(warehouseID) && g.PartNumber.Equals(partNumber) && g.PalletID.Equals(palletID) && g.IDCode.Equals(idCode));
            return await entrys.ToListAsync();
        }
    }
}
