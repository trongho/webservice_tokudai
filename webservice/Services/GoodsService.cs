﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class GoodsService : IGoodsService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public GoodsService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }

        public async Task<bool> Create(Goods goods)
        {
            var entrys = warehouseDbContext.Goods.AddAsync(goods);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string partNumber, string palletID)
        {
            var entrys = warehouseDbContext.Goods.FirstOrDefault(o => o.PartNumber.Equals(partNumber)&&o.PalletID.Equals(palletID));
            warehouseDbContext.Goods.Remove(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<bool> Delete()
        {
            var entrys = warehouseDbContext.Goods;
            warehouseDbContext.Goods.RemoveRange(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<Goods>> GetAllGoods()
        {
            var entrys = warehouseDbContext.Goods;
            return await entrys.ToListAsync();
        }

        public async Task<List<Goods>> GetUnderId(string partNumber, string palletID)
        {
            var entrys = warehouseDbContext.Goods.Where(u => u.PartNumber.Equals(partNumber) && u.PalletID.Equals(palletID));
            return await entrys.ToListAsync();
        }

        public async Task<List<Goods>> GetUnderId(string palletID)
        {
            var entrys = warehouseDbContext.Goods.Where(u =>u.PalletID.Equals(palletID));
            return await entrys.ToListAsync();
        }

        public async Task<List<Goods>> GetUnderId2(string partNumber)
        {
            var entrys = warehouseDbContext.Goods.Where(u => u.PartNumber.Equals(partNumber));
            return await entrys.ToListAsync();
        }

        public async Task<bool> Update(string partNumber, string palletID, Goods goods)
        {
            var entrys = warehouseDbContext.Goods.FirstOrDefault(o => o.PartNumber.Equals(partNumber)&&o.PalletID.Equals(palletID));
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(goods);
            warehouseDbContext.SaveChanges();
            return true;
        }

    }
}
