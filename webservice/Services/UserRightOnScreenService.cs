﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class UserRightOnScreenService : IUserRightOnScreenService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public UserRightOnScreenService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }

        public async Task<bool> checkExist(string userId, string screenId)
        {
            Boolean flag = false;
            var entrys = warehouseDbContext.UserRightOnScreens.FirstOrDefault(g => g.UserID.Equals(userId) && g.ScreenID.Equals(screenId));
            if (entrys != null)
            {
                flag = true;
            }
            return flag;
        }

        public async Task<bool> CreateUserRightOnScreen(UserRightOnScreen userRightOnScreen)
        {
            var entry = warehouseDbContext.UserRightOnScreens.AddAsync(userRightOnScreen);
            warehouseDbContext.SaveChanges();

            return entry.IsCompleted;
        }

        public async Task<bool> DeleteUserRightOnScreen(string userId, string screenId)
        {
            var entry = warehouseDbContext.UserRightOnScreens.FirstOrDefault(o => o.UserID.Equals(userId) && o.ScreenID.Equals(screenId));
            warehouseDbContext.UserRightOnScreens.Remove(entry);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<UserRightOnScreen>> GetRightByScreenId(string id)
        {
            var entrys = warehouseDbContext.UserRightOnScreens.Where(ur => ur.ScreenID.Equals(id));

            return await entrys.ToListAsync();
        }

        public async Task<List<UserRightOnScreen>> GetRightByUserId(string id)
        {
            var entrys = warehouseDbContext.UserRightOnScreens.Where(ur => ur.UserID.Equals(id));

            return await entrys.ToListAsync();
        }

        public async Task<List<UserRightOnScreen>> GetRightByUserScreentId(string userId, string screenId)
        {
            var entrys= warehouseDbContext.UserRightOnScreens.Where(ur => ur.UserID.Equals(userId) && ur.ScreenID.Equals(screenId));

            return await entrys.ToListAsync();
        }

        public async Task<bool> UpdateUserRightOnScreen(string userId, string screenId, UserRightOnScreen userRightOnScreen)
        {
            var entrys= warehouseDbContext.UserRightOnScreens.FirstOrDefault(o => o.UserID.Equals(userId) && o.ScreenID.Equals(screenId));
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(userRightOnScreen);
            warehouseDbContext.SaveChanges();
            return true;
        }
    }
}
