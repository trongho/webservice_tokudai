﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class InventoryDetailService : IInventoryDetailService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public InventoryDetailService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }
        public async Task<bool> Create(InventoryDetail InventoryDetail)
        {
            var entrys = warehouseDbContext.InventoryDetails.AddAsync(InventoryDetail);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(short year, short month, string warehouseID, string partNumber, string palletID)
        {
            var entrys = warehouseDbContext.InventoryDetails.Where(g => g.Year == year
            && g.Month == month && g.WarehouseID.Equals(warehouseID) && g.PartNumber.Equals(partNumber)&&g.PalletID.Equals(palletID)).FirstOrDefault();
            warehouseDbContext.InventoryDetails.Remove(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<bool> Delete(short year, short month, string warehouseID, string partNumber)
        {
            var entrys = warehouseDbContext.InventoryDetails.Where(g => g.Year == year
            && g.Month == month && g.WarehouseID.Equals(warehouseID) && g.PartNumber.Equals(partNumber));
            warehouseDbContext.InventoryDetails.RemoveRange(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<bool> Delete(short year, short month, string warehouseID)
        {
            var entrys = warehouseDbContext.InventoryDetails.Where(g => g.Year == year
            && g.Month == month && g.WarehouseID.Equals(warehouseID));
            warehouseDbContext.InventoryDetails.RemoveRange(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<bool> Delete(short year, short month, string warehouseID, string partNumber, string palletID, string idCode)
        {
            var entrys = warehouseDbContext.InventoryDetails.Where(g => g.Year == year
            && g.Month == month && g.WarehouseID.Equals(warehouseID) && g.PartNumber.Equals(partNumber) && g.PalletID.Equals(palletID)&&g.IDCode.Equals(idCode)).FirstOrDefault();
            warehouseDbContext.InventoryDetails.Remove(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<bool> DeleteOldDate(string warehouseID)
        {
            var entrys = warehouseDbContext.InventoryDetails.Where(g => ((g.Year == System.DateTime.Now.Year && g.Month < System.DateTime.Now.Month) || g.Year < System.DateTime.Now.Year)
            && g.WarehouseID.Equals(warehouseID));
            warehouseDbContext.InventoryDetails.RemoveRange(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<InventoryDetail>> GetAll()
        {
            var entrys = warehouseDbContext.InventoryDetails;
            return await entrys.ToListAsync();
        }

        public async Task<List<InventoryDetail>> GetUnderId(short year, short month, string warehouseID, string partNumber)
        {
            var entrys = warehouseDbContext.InventoryDetails.Where(g => g.Year == year
            && g.Month == month && g.WarehouseID.Equals(warehouseID) && g.PartNumber.Equals(partNumber));
            return await entrys.ToListAsync();
        }

        public async Task<List<InventoryDetail>> GetUnderId(short year, short month, string warehouseID, string partNumber, string palletID)
        {
            var entrys = warehouseDbContext.InventoryDetails.Where(g => g.Year == year
            && g.Month == month && g.WarehouseID.Equals(warehouseID) && g.PartNumber.Equals(partNumber)&&g.PalletID.Equals(palletID));
            return await entrys.ToListAsync();
        }

        public async Task<List<InventoryDetail>> GetUnderId2(short year, short month, string warehouseID, string partNumber, string palletID, string idCode)
        {
            var entrys = warehouseDbContext.InventoryDetails.Where(g => g.Year == year
            && g.Month == month && g.WarehouseID.Equals(warehouseID) && g.PartNumber.Equals(partNumber) && g.PalletID.Equals(palletID)&&g.IDCode.Equals(idCode));
            return await entrys.ToListAsync();
        }

        public async Task<List<InventoryDetail>> GetUnderId(short year, short month, string warehouseID, string partNumber, string palletID, string lotID)
        {
            var entrys = warehouseDbContext.InventoryDetails.Where(g => g.Year == year
           && g.Month == month && g.WarehouseID.Equals(warehouseID) && g.PartNumber.Equals(partNumber) && g.PalletID.Equals(palletID) && g.LotID.Equals(lotID));
            return await entrys.ToListAsync();
        }

        public async Task<bool> Update(short year, short month, string warehouseID, string partNumber, string palletID, InventoryDetail InventoryDetail)
        {
            var entrys = warehouseDbContext.InventoryDetails.FirstOrDefault(g => g.Year == year
            && g.Month == month && g.WarehouseID.Equals(warehouseID) && g.PartNumber.Equals(partNumber)&&g.PalletID.Equals(palletID));
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(InventoryDetail);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<InventoryDetail>> GetUnderId(short year, short month, string warehouseID)
        {
            var entrys = warehouseDbContext.InventoryDetails.Where(g => g.Year == year
           && g.Month == month && g.WarehouseID.Equals(warehouseID));
            return await entrys.ToListAsync();
        }

        public async Task<List<InventoryDetail>> GetUnderId(string warehouseID, string partNumber, string palletID, string idCode)
        {
            var entrys = warehouseDbContext.InventoryDetails.Where(g=>g.WarehouseID.Equals(warehouseID) && g.PartNumber.Equals(partNumber) && g.PalletID.Equals(palletID) && g.IDCode.Equals(idCode));
            return await entrys.ToListAsync();
        }

        public async Task<List<InventoryDetail>> GetUnderId(string warehouseID)
        {
            var entrys = warehouseDbContext.InventoryDetails.Where(g => g.WarehouseID.Equals(warehouseID));
            return await entrys.ToListAsync();
        }

        public async Task<List<InventoryDetail>> GetUnderPartNumber(string partNumber)
        {
            var entrys = warehouseDbContext.InventoryDetails.Where(g => g.PartNumber.Equals(partNumber));
            return await entrys.ToListAsync();
        }

        public async Task<List<InventoryDetail>> GetUnderWarehouseAnPallet(short year, short month, string warehouseID, string palletID)
        {
            var entrys = warehouseDbContext.InventoryDetails.Where(g => g.Year == year
            && g.Month == month && g.WarehouseID.Equals(warehouseID) && g.PalletID.Equals(palletID));
            return await entrys.ToListAsync();
        }

        public async Task<List<InventoryDetail>> GetUnderIDCode(string idCode)
        {
            var entrys = warehouseDbContext.InventoryDetails.Where(g => g.IDCode.Equals(idCode));
            return await entrys.ToListAsync();
        }
    }
}
