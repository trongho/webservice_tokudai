﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class WIRDetailService:IWIRDetailService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public WIRDetailService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }

        public async Task<bool> Create(WIRDetail wIRDetail)
        {
            var entrys = warehouseDbContext.WIRDetails.AddAsync(wIRDetail);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string id)
        {
            var entrys = warehouseDbContext.WIRDetails.Where(o => o.WIRNumber.Equals(id));
            warehouseDbContext.WIRDetails.RemoveRange(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<bool> Delete(string wRRNumber, string palletID,string partNumber, int Ordinal)
        {
            var entrys = warehouseDbContext.WIRDetails.Where(o => o.WIRNumber.Equals(wRRNumber) && o.PalletID.Equals(palletID)&&o.PartNumber.Equals(partNumber) && o.Ordinal == Ordinal).FirstOrDefault();
            warehouseDbContext.WIRDetails.Remove(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

    

        public async Task<List<WIRDetail>> GetUnderId(string wIRNumber)
        {
            var entrys = warehouseDbContext.WIRDetails.Where(u => u.WIRNumber.Equals(wIRNumber));
            return await entrys.ToListAsync();
        }

        public async Task<List<WIRDetail>> GetUnderId(string wIRNumber, string palletID,string partNumber)
        {
            var entrys = warehouseDbContext.WIRDetails.Where(u => u.WIRNumber.Equals(wIRNumber) && u.PalletID.Equals(palletID)&&u.PartNumber.Equals(partNumber));
            return await entrys.ToListAsync();
        }


        public async Task<bool> Update(string wIRNumber, string palletID,string partNumber, int Ordinal, WIRDetail wIRDetail)
        {
            var entrys = warehouseDbContext.WIRDetails.FirstOrDefault(o => o.WIRNumber.Equals(wIRNumber) && o.PalletID.Equals(palletID)&&o.PartNumber.Equals(partNumber) && o.Ordinal == Ordinal);
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(wIRDetail);
            warehouseDbContext.SaveChanges();
            return true;
        }
    }
}
