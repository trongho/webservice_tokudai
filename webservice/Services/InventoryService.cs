﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class InventoryService : IInventoryService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public InventoryService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }

        public async Task<bool> checkExist(short year, short month, string warehouseID)
        {
            Boolean flag = false;
            var entrys = warehouseDbContext.Inventorys.FirstOrDefault(g => g.Year==year
            &&g.Month==month&&g.WarehouseID.Equals(warehouseID));
            if (entrys != null)
            {
                flag = true;
            }
            return flag;
        }

        public async Task<bool> checkExist(short year, short month, string warehouseID, string PartNumber)
        {
            Boolean flag = false;
            var entrys = warehouseDbContext.Inventorys.FirstOrDefault(g => g.Year == year
            && g.Month == month && g.WarehouseID.Equals(warehouseID) && g.PartNumber.Equals(PartNumber));
            if (entrys != null)
            {
                flag = true;
            }
            return flag;
        }

        public async Task<bool> Create(Inventory inventory)
        {
            var entrys = warehouseDbContext.Inventorys.AddAsync(inventory);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(short year, short month, string warehouseID, string PartNumber)
        {
            var entrys = warehouseDbContext.Inventorys.Where(g => g.Year==year
            && g.Month == month && g.WarehouseID.Equals(warehouseID) && g.PartNumber.Equals(PartNumber)).FirstOrDefault();
            warehouseDbContext.Inventorys.Remove(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<bool> Delete(short year, short month, string warehouseID)
        {
            var entrys = warehouseDbContext.Inventorys.Where(g => g.Year == year
            && g.Month == month && g.WarehouseID.Equals(warehouseID));
            warehouseDbContext.Inventorys.RemoveRange(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<bool> DeleteOldDate(string warehouseID)
        {
            var entrys = warehouseDbContext.Inventorys.Where(g => ((g.Year==System.DateTime.Now.Year&&g.Month< System.DateTime.Now.Month)||g.Year<System.DateTime.Now.Year)
            && g.WarehouseID.Equals(warehouseID));
            warehouseDbContext.Inventorys.RemoveRange(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<Inventory>> GetAll()
        {
            var entrys = warehouseDbContext.Inventorys;
            return await entrys.ToListAsync();
        }

        public async Task<List<Inventory>> GetUnderId(short year, short month, string warehouseID)
        {
            var entrys = warehouseDbContext.Inventorys.Where(g => g.Year == year
            && g.Month == month && g.WarehouseID.Equals(warehouseID));
            return await entrys.ToListAsync();
        }

        public async Task<List<Inventory>> GetUnderId(short year, short month, string warehouseID, string PartNumber)
        {
            var entrys = warehouseDbContext.Inventorys.Where(g => g.Year == year
            && g.Month == month && g.WarehouseID.Equals(warehouseID) && g.PartNumber.Equals(PartNumber));
            return await entrys.ToListAsync();
        }

        public async Task<List<Inventory>> GetUnderId(string warehouseID, string partNumber)
        {
            var entrys = warehouseDbContext.Inventorys.Where(g => g.WarehouseID.Equals(warehouseID)&&g.PartNumber.Equals(partNumber));
            return await entrys.ToListAsync();
        }

        public async Task<List<Inventory>> GetUnderId(string warehouseID)
        {
            var entrys = warehouseDbContext.Inventorys.Where(g => g.WarehouseID.Equals(warehouseID));
            return await entrys.ToListAsync();
        }

        public async Task<bool> Update(short year, short month, string warehouseID, string PartNumber, Inventory inventory)
        {
            var entrys = warehouseDbContext.Inventorys.FirstOrDefault(g => g.Year == year
            && g.Month == month && g.WarehouseID.Equals(warehouseID) && g.PartNumber.Equals(PartNumber));
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(inventory);
            warehouseDbContext.SaveChanges();
            return true;
        }
    }
}
