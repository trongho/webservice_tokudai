﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class WRDataGeneralService: IWRDataGeneralService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public WRDataGeneralService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }

        public async Task<bool> Create(WRDataGeneral WRDataGeneral)
        {
            var entrys =warehouseDbContext.WRDataGenerals.AddAsync(WRDataGeneral);
            warehouseDbContext.SaveChanges();

            return  entrys.IsCompleted;
        }

        public async Task<bool> Delete(string wRDNumber)
        {
            var entrys = warehouseDbContext.WRDataGenerals.Where(o => o.WRDNumber.Equals(wRDNumber));
            warehouseDbContext.WRDataGenerals.RemoveRange(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<bool> Delete(string wRDNumber, string palletID, string lotID)
        {
            var entrys = warehouseDbContext.WRDataGenerals.Where(o => o.WRDNumber.Equals(wRDNumber)&&o.PalletID.Equals(palletID)&&o.LotID.Equals(lotID)).FirstOrDefault();
            warehouseDbContext.WRDataGenerals.Remove(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<bool> Delete(string wRDNumber, string palletID)
        {
            var entrys = warehouseDbContext.WRDataGenerals.Where(o => o.WRDNumber.Equals(wRDNumber) && o.PalletID.Equals(palletID)).FirstOrDefault();
            warehouseDbContext.WRDataGenerals.Remove(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<WRDataGeneral>> GetAll()
        {
            var entrys = warehouseDbContext.WRDataGenerals;
            return await entrys.ToListAsync();
        }

        public async Task<List<WRDataGeneral>> GetUnderId(string wRDNumber)
        {
            var entrys = warehouseDbContext.WRDataGenerals.Where(u => u.WRDNumber.Equals(wRDNumber));
            return await entrys.ToListAsync();
        }

        public async Task<List<WRDataGeneral>> GetUnderId(string wRDNumber, string palletID, int Ordinal)
        {
            var entrys = warehouseDbContext.WRDataGenerals.Where(u => u.WRDNumber.Equals(wRDNumber) && u.PalletID.Equals(palletID) && u.Ordinal==Ordinal);
            return await entrys.ToListAsync();
        }

        public async Task<List<WRDataGeneral>> GetUnderId(string wRDNumber, string palletID)
        {
            var entrys = warehouseDbContext.WRDataGenerals.Where(u => u.WRDNumber.Equals(wRDNumber) && u.PalletID.Equals(palletID));
            return await entrys.ToListAsync();
        }

        public async Task<List<WRDataGeneral>> GetUnderId(string wRDNumber, string palletID, int Ordinal, string lotID)
        {
            var entrys = warehouseDbContext.WRDataGenerals.Where(u => u.WRDNumber.Equals(wRDNumber) && u.PalletID.Equals(palletID) && u.Ordinal == Ordinal&&u.LotID.Equals(lotID));
            return await entrys.ToListAsync();
        }

        public async Task<List<WRDataGeneral>> GetUnderId(string wRDNumber, string palletID, string lotID)
        {
            var entrys = warehouseDbContext.WRDataGenerals.Where(u => u.WRDNumber.Equals(wRDNumber) && u.PalletID.Equals(palletID) && u.LotID.Equals(lotID));
            return await entrys.ToListAsync();
        }

        public async Task<bool> Update(string wRDNumber, string palletID, int Ordinal, string lotID, WRDataGeneral wRDataGeneral)
        {
            var entrys = warehouseDbContext.WRDataGenerals.FirstOrDefault(o => o.WRDNumber.Equals(wRDNumber) && o.PalletID.Equals(palletID) && o.Ordinal == Ordinal&&o.LotID.Equals(lotID));
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(wRDataGeneral);
            warehouseDbContext.SaveChanges();
            return true;
        }
    }
}
