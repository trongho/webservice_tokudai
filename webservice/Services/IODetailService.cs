﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public  class IODetailservice : IIODetailService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public  IODetailservice(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }

        public async Task<bool> Create(IODetail iODetail)
        {
            var entrys = warehouseDbContext.IODetails.AddAsync(iODetail);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string IONumber)
        {
            var entrys = warehouseDbContext.IODetails.Where(o => o.IONumber.Equals(IONumber));
            warehouseDbContext.IODetails.RemoveRange(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<bool> Delete(string IONumber, string palletID, string partNumber, int Ordinal)
        {
            var entrys = warehouseDbContext.IODetails.Where(o => o.IONumber.Equals(IONumber)&&o.PalletID.Equals(palletID)&&o.PartNumber.Equals(partNumber)&&o.Ordinal==Ordinal).FirstOrDefault();
            warehouseDbContext.IODetails.Remove(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

       

        public async Task<List<IODetail>> GetUnderId(string id)
        {
            var entrys = warehouseDbContext.IODetails.Where(u => u.IONumber.Equals(id));
            return await entrys.ToListAsync();
        }


        public async Task<List<IODetail>> GetUnderId(string IONumber, string palletID, string partNumber)
        {
            var entrys = warehouseDbContext.IODetails.Where(u => u.IONumber.Equals(IONumber)&&u.PalletID.Equals(palletID) && u.PartNumber.Equals(partNumber));
            return await entrys.ToListAsync();
        }


        public async Task<bool> Update(string IONumber, string palletID, string partNumber, int Ordinal, IODetail iODetail)
        {
            var entrys = warehouseDbContext.IODetails.FirstOrDefault(o => o.IONumber.Equals(IONumber)&&o.PalletID.Equals(palletID) && o.PartNumber.Equals(partNumber) && o.Ordinal == Ordinal);
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(iODetail);
            warehouseDbContext.SaveChanges();
            return true;
        }
    }
}
