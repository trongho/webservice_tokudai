﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class GGDataDetailService : IGGDataDetailService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public GGDataDetailService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }

        public async Task<bool> checkExist(string GGDNumber, string goodIDs)
        {
            Boolean flag = false;
            var entrys = warehouseDbContext.GGDataDetails.FirstOrDefault(g => g.GGDNumber.Equals(GGDNumber)&&g.GoodsID.Equals(goodIDs));
            if (entrys != null)
            {
                flag = true;
            }
            return flag;
        }

        public async Task<bool> Create(GGDataDetail GGDataDetail)
        {
            var entrys = warehouseDbContext.GGDataDetails.AddAsync(GGDataDetail);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string GGDNumber)
        {
            var entrys = warehouseDbContext.GGDataDetails.Where(o => o.GGDNumber.Equals(GGDNumber));
            warehouseDbContext.GGDataDetails.RemoveRange(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<GGDataDetail>> GetAll()
        {
            var entrys = warehouseDbContext.GGDataDetails;
            return await entrys.ToListAsync();
        }

        public async Task<string> GetLastId()
        {
            var curentID = warehouseDbContext.GGDataDetails.OrderByDescending(x => x.GGDNumber).Take(1).Select(x => x.GGDNumber).ToList().FirstOrDefault();
            return curentID;
        }

        public async Task<List<GGDataDetail>> GetUnderId(string GGDNumber)
        {
            var entrys = warehouseDbContext.GGDataDetails.Where(u => u.GGDNumber.Equals(GGDNumber));
            return await entrys.ToListAsync();
        }

        public async Task<List<GGDataDetail>> GetUnderId(string GGDNumber, string goodsID, int Ordinal)
        {
            var entrys = warehouseDbContext.GGDataDetails.Where(u => u.GGDNumber.Equals(GGDNumber) && u.GoodsID.Equals(goodsID) && u.Ordinal == Ordinal);
            return await entrys.ToListAsync();
        }

        public async Task<List<GGDataDetail>> GetUnderIdCode(string GGDNumber, string goodsID, string IDCode)
        {
            int idcodeLenght = IDCode.Length;
            int percentIndex = IDCode.IndexOf("%");
            String s1 = IDCode.Substring(0, 1);
            String s2 = IDCode.Substring(percentIndex, 3);
            String s3 = IDCode.Substring(percentIndex + 3, idcodeLenght - s1.Length - s2.Length);

            s2 = s2.Replace(s2, "/");
            IDCode = s1 + s2 + s3;

            var entrys = warehouseDbContext.GGDataDetails.Where(u => u.GGDNumber.Equals(GGDNumber) && u.GoodsID.Equals(goodsID) && u.IDCode == IDCode);
            return await entrys.ToListAsync();
        }

        public async Task<bool> Update(string GGDNumber, string goodsID, int Ordinal, GGDataDetail GGDataDetail)
        {
            var entrys = warehouseDbContext.GGDataDetails.FirstOrDefault(o => o.GGDNumber.Equals(GGDNumber) && o.GoodsID.Equals(goodsID) && o.Ordinal == Ordinal);
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(GGDataDetail);
            warehouseDbContext.SaveChanges();
            return true;
        }
    }
}
