﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class WIDataDetailService : IWIDataDetailService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public WIDataDetailService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }


        public async Task<bool> Create(WIDataDetail WIDataDetail)
        {
            var entrys = warehouseDbContext.WIDataDetails.AddAsync(WIDataDetail);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string wIDNumber)
        {
            var entrys = warehouseDbContext.WIDataDetails.Where(o => o.WIDNumber.Equals(wIDNumber));
            warehouseDbContext.WIDataDetails.RemoveRange(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<bool> Delete(string wIDNumber,String palletID, string partNumber,int ordinal ,string lotID, string idCode)
        {
            var entrys = warehouseDbContext.WIDataDetails.Where(o => o.WIDNumber.Equals(wIDNumber)&&o.PalletID.Equals(palletID) &&o.PartNumber.Equals(partNumber)&&o.Ordinal==ordinal
                &&o.LotID.Equals(lotID)&&o.IDCode.Equals(idCode)).FirstOrDefault();
            warehouseDbContext.WIDataDetails.Remove(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }


        public async Task<List<WIDataDetail>> GetUnderId(string wIDNumber)
        {
            var entrys = warehouseDbContext.WIDataDetails.Where(u => u.WIDNumber.Equals(wIDNumber));
            return await entrys.ToListAsync();
        }

        public async Task<List<WIDataDetail>> GetUnderId(string wIDNumber,string palletID, string PartNumber,string LotID, string IDCode)
        {
            var entrys = warehouseDbContext.WIDataDetails.Where(u => u.WIDNumber.Equals(wIDNumber)&&u.PalletID.Equals(palletID) && u.PartNumber.Equals(PartNumber)
                &&u.LotID.Equals(LotID)&& u.IDCode == IDCode);
            return await entrys.ToListAsync();
        }

        public async Task<List<WIDataDetail>> GetUnderId(string wIDNumber,string palletID, string PartNumber)
        {
            var entrys = warehouseDbContext.WIDataDetails.Where(u => u.WIDNumber.Equals(wIDNumber) && u.PartNumber.Equals(PartNumber) &&u.PalletID.Equals(palletID));
            return await entrys.ToListAsync();
        }

        

        public async Task<bool> Update(string wIDNumber,string palletID, string partNumber, int Ordinal, string lotID, string idCode, WIDataDetail WIDataDetail)
        {
            var entrys = warehouseDbContext.WIDataDetails.FirstOrDefault(o => o.WIDNumber.Equals(wIDNumber) &&o.PalletID.Equals(palletID)&& o.PartNumber.Equals(partNumber) && o.Ordinal == Ordinal
                &&o.LotID.Equals(lotID)&&o.IDCode.Equals(idCode));
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(WIDataDetail);
            warehouseDbContext.SaveChanges();
            return true;
        }
    }
}
