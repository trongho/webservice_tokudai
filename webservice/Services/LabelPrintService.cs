﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class LabelPrintService : ILabelPrintService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public LabelPrintService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }

        public async Task<bool> Create(LabelPrint labelPrint)
        {
            var entrys = warehouseDbContext.LabelPrints.AddAsync(labelPrint);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string WRRNumber, string palletID, string partNumber, string lotID)
        {
            var entrys = warehouseDbContext.LabelPrints.Where(g => g.WRRNumber.Equals(WRRNumber)&&g.PalletID.Equals(palletID)&&
                g.PartNumber.Equals(partNumber)&&g.LotID.Equals(lotID)).FirstOrDefault();
            warehouseDbContext.LabelPrints.Remove(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<LabelPrint>> GetUnderID(string WRRNumber, string palletID, string partNumber, string lotID,string idCode)
        {
            var entrys = warehouseDbContext.LabelPrints.Where(g=>g.WRRNumber.Equals(WRRNumber) && g.PalletID.Equals(palletID) &&
                g.PartNumber.Equals(partNumber) && g.LotID.Equals(lotID)&&g.IDCode.Equals(idCode));
            return await entrys.ToListAsync();
        }

        public async Task<List<LabelPrint>> GetUnderID(string WRRNumber)
        {
            var entrys = warehouseDbContext.LabelPrints.Where(g => g.WRRNumber.Equals(WRRNumber));
            return await entrys.ToListAsync();
        }

        public async Task<List<LabelPrint>> GetUnderID(string WRRNumber, string palletID, string partNumber, string lotID)
        {
            var entrys = warehouseDbContext.LabelPrints.Where(g => g.WRRNumber.Equals(WRRNumber) && g.PalletID.Equals(palletID) &&
               g.PartNumber.Equals(partNumber) && g.LotID.Equals(lotID));
            return await entrys.ToListAsync();
        }

        public async Task<bool> Update(LabelPrint labelPrint, string WRRNumber, string palletID, string partNumber, string lotID, string idCode)
        {
            var entrys = warehouseDbContext.LabelPrints.FirstOrDefault(g => g.WRRNumber.Equals(WRRNumber) && g.PalletID.Equals(palletID) &&
                g.PartNumber.Equals(partNumber) && g.LotID.Equals(lotID));
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(labelPrint);
            warehouseDbContext.SaveChanges();
            return true;
        }
    }
}
