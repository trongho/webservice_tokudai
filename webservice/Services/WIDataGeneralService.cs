﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class WIDataGeneralService: IWIDataGeneralService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public WIDataGeneralService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }


        public async Task<bool> Create(WIDataGeneral WIDataGeneral)
        {
            var entrys = warehouseDbContext.WIDataGenerals.AddAsync(WIDataGeneral);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string wIDNumber)
        {
            var entrys = warehouseDbContext.WIDataGenerals.Where(o => o.WIDNumber.Equals(wIDNumber));
            warehouseDbContext.WIDataGenerals.RemoveRange(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }



        public async Task<bool> Delete(string wIDNumber, string palletID, string partNumber, int ordinal)
        {
            var entrys = warehouseDbContext.WIDataGenerals.Where(o => o.WIDNumber.Equals(wIDNumber) && o.PartNumber.Equals(partNumber) && o.PalletID.Equals(palletID)&&o.Ordinal==ordinal).FirstOrDefault();
            warehouseDbContext.WIDataGenerals.Remove(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }


        public async Task<List<WIDataGeneral>> GetUnderId(string wIDNumber)
        {
            var entrys = warehouseDbContext.WIDataGenerals.Where(u => u.WIDNumber.Equals(wIDNumber));
            return await entrys.ToListAsync();
        }


        public async Task<List<WIDataGeneral>> GetUnderId(string wIDNumber, string palletID, string partNumber)
        {
            var entrys = warehouseDbContext.WIDataGenerals.Where(u => u.WIDNumber.Equals(wIDNumber) && u.PartNumber.Equals(partNumber)&&u.PalletID.Equals(palletID));
            return await entrys.ToListAsync();
        }

        //public async Task<List<WIDataGeneral>> GetUnderIdCode(string wIDNumber, string PartNumber, string IDCode)
        //{
        //    int idcodeLenght = IDCode.Length;
        //    int percentIndex = IDCode.IndexOf("%");
        //    String s1 = IDCode.Substring(0, 1);
        //    String s2 = IDCode.Substring(percentIndex, 3);
        //    String s3 = IDCode.Substring(percentIndex + 3, idcodeLenght - s1.Length - s2.Length);

        //    s2 = s2.Replace(s2, "/");
        //    IDCode = s1 + s2 + s3;

        //    var entrys = warehouseDbContext.WIDataGenerals.Where(u => u.WIDNumber.Equals(wIDNumber) && u.PartNumber.Equals(PartNumber) && u.IDCode == IDCode);
        //    return await entrys.ToListAsync();
        //}

        //public async Task<bool> Update(string wIDNumber, string palletID, int Ordinal,String lotID, WIDataGeneral WIDataGeneral)
        //{
        //    var entrys = warehouseDbContext.WIDataGenerals.FirstOrDefault(o => o.WIDNumber.Equals(wIDNumber) && o.PalletID.Equals(palletID) && o.Ordinal == Ordinal&&o.LotID.Equals(lotID));
        //    warehouseDbContext.Entry(entrys).CurrentValues.SetValues(WIDataGeneral);
        //    warehouseDbContext.SaveChanges();
        //    return true;
        //}



        public async Task<bool> Update(string wIDNumber, string palletID, string partNumber, int Ordinal, WIDataGeneral WIDataGeneral)
        {
            var entrys = warehouseDbContext.WIDataGenerals.FirstOrDefault(o => o.WIDNumber.Equals(wIDNumber) && o.PalletID.Equals(palletID) && o.Ordinal == Ordinal && o.PartNumber.Equals(partNumber));
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(WIDataGeneral);
            warehouseDbContext.SaveChanges();
            return true;
        }
    }
}
