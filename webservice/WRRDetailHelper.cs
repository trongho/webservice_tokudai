﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class WRRDetailHelper
    {
        public static List<WRRDetailModel> Covert(List<WRRDetail> entrys)
        {
            var models = entrys.ConvertAll(sc => new WRRDetailModel
            {
                  WRRNumber=sc.WRRNumber,
                  Ordinal=sc.Ordinal,
                  PalletID=sc.PalletID,
                  PartNumber=sc.PartNumber,
                  PartName=sc.PartName,
                  ProductFamily=sc.ProductFamily,
                  LotID=sc.LotID,
                  QuantityByItem=sc.QuantityByItem,
                  QuantityByPack=sc.QuantityByPack,
                  PackingVolume=sc.PackingVolume,
                  TotalQuantity=sc.TotalQuantity,
                  Unit=sc.Unit,
                  MFGDate=sc.MFGDate,
                  EXPDate=sc.EXPDate,
                  PONumber=sc.PONumber,
                  Remark=sc.Remark,
                  Status=sc.Status,
                  QuantityByPrinted=sc.QuantityByPrinted,
                  RemainingPrintQuantity=sc.RemainingPrintQuantity,
                  IDCodeRange=sc.IDCodeRange,
            });

            return models;
        }
    }
}
