﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface IWIRDetailService
    {
        Task<List<WIRDetail>> GetUnderId(String wIRNumber);
        Task<List<WIRDetail>> GetUnderId(String wIRNumber, String palletID,String partNumber);
        Task<Boolean> Create(WIRDetail wIRDetail);
        Task<Boolean> Update(String wIRNumber, String palletID,String partNumber, int Ordinal, WIRDetail wIRDetail);
        Task<Boolean> Delete(String wIRNumber);
        Task<Boolean> Delete(String wRRNumber, String palletID, String partNumber,int Ordinal);      
    }
}
