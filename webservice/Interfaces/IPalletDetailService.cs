﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface IPalletDetailService
    {
        Task<List<PalletDetail>> GetAll();
        Task<List<PalletDetail>> GetUnderIDCode(String idCode);
        Task<List<PalletDetail>> GetUnderId(String palletID);
        Task<List<PalletDetail>> GetUnderId(String palletID,String idCode);
        Boolean Create(PalletDetail PalletDetail);
        Boolean Update(String palletID,int ordinal,String idCode, PalletDetail PalletDetail);
        Boolean Delete(String palletID);
        Boolean Delete(String palletID,String idCode);
    }
}
