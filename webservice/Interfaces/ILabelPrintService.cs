﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface ILabelPrintService
    {
        Task<List<LabelPrint>> GetUnderID(string WRRNumber, string palletID, string partNumber, string lotID,string idCode);
        Task<List<LabelPrint>> GetUnderID(string WRRNumber);
        Task<List<LabelPrint>> GetUnderID(string WRRNumber, string palletID, string partNumber, string lotID);
        Task<bool> Create(LabelPrint labelPrint);
        Task<bool> Update(LabelPrint labelPrint,string WRRNumber, string palletID, string partNumber, string lotID,string idCode);
        Task<bool> Delete(string WRRNumber, string palletID, string partNumber, string lotID);
    }
}
