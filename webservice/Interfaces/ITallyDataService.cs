﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface ITallyDataService
    {
        Task<Boolean> Create(TallyData entry);
        Task<List<TallyData>> GetUnderId(String id);
        Task<List<TallyData>> GetUnderId(String tsNumber,String partNumber,int No);
        Task<Boolean> Update(String tsNumber,String partNumber, int No,TallyData entry);
        Task<Boolean> Delete(String tsNumber);
        Task<Boolean> Delete(String tsNumber,String partNumber, int No);
        Task<String> GetLastId();
    }
}
