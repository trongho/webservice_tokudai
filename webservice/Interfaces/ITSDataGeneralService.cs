﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface ITSDataGeneralService
    {
        Task<Boolean> Create(TSDataGeneral entry);
        Task<List<TSDataGeneral>> GetUnderId(String warehouseID,DateTime TallyDate);
        Task<List<TSDataGeneral>> GetMultiId(String warehouseID,DateTime tallyDate,String goodsID);
        Task<Boolean> Update(String warehouseID, DateTime tallyDate, int Ordinal, String goodsID, TSDataGeneral entry);
        Task<Boolean> Delete(String warehouseID,DateTime tallyDate);
    }
}
