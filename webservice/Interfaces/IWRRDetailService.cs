﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface IWRRDetailService
    {
        Task<List<WRRDetail>> GetAll();
        Task<List<WRRDetail>> GetUnderId(String wRRNumber,String palletID,int Ordinal);
        Task<List<WRRDetail>> GetUnderId(String wRRNumber, String palletID,String lotID);
        Task<List<WRRDetail>> GetUnderId(String wRRNumber, String palletID);
        Task<List<WRRDetail>> GetUnderId(String wRRNumber);
        Task<Boolean> Create(WRRDetail wRRDetail);
        Task<Boolean> Update(String wRRNumber, String palletID, int Ordinal,String lotID, WRRDetail wRRDetail);
        Task<Boolean> Delete(String wRRNumber);
        Task<Boolean> Delete(String wRRNumber,String palletID,int Ordinal);
    }
}
