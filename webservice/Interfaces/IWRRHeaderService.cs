﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface IWRRHeaderService
    {
        Task<List<WRRHeader>> GetAll();
        Task<List<WRRHeader>> GetUnderId(String id);
        Task<Boolean> Create(WRRHeader wRRHeader);
        Task<Boolean> Update(String id, WRRHeader wRRHeader);
        Task<Boolean> Delete(String id);
        Task<String> GetLastId();
        Task<Boolean> checkExist(String id);
        Task<List<WRRHeader>> GetUnderDate(DateTime fromDate,DateTime toDate);
        Task<List<WRRHeader>> GetUnderBranchID(String branchID);
        Task<List<WRRHeader>> GetUnderHandlingStatusID(String handlingStatusID);
        Task<List<WRRHeader>> Filter(String branchID, DateTime fromDate, DateTime toDate, String handlingStatusID);
        Task<List<WRRHeader>> FilterByDateAndHandlingStatus(DateTime fromDate, DateTime toDate, String handlingStatusID);
        Task<List<WRRHeader>> FilterByDateAndBranch(DateTime fromDate, DateTime toDate, String branchID);
    }
}
