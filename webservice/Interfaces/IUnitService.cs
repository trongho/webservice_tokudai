﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface IUnitService
    {
        Task<List<Unit>> GetAll();
        Task<Boolean> Create(Unit entry);
        Task<List<Unit>> GetUnderId(String id);
        Task<Boolean> Update(String id, Unit entry);
        Task<Boolean> Delete(String id);
        Task<Boolean> checkExist(String id);
    }
}
