﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface IWIDataDetailService
    {
        Task<List<WIDataDetail>> GetUnderId(String wIDNumber);
        Task<List<WIDataDetail>> GetUnderId(String wIDNumber,String palletID,String PartNumber);
        Task<List<WIDataDetail>> GetUnderId(String wIDNumber,String palletID, String PartNumber,String LotID,String IDCode);
        Task<Boolean> Create(WIDataDetail WIDataDetail);
        Task<Boolean> Update(String wIDNumber,String palletID, String partNumber, int Ordinal,String lotID,String idCode, WIDataDetail WIDataDetail);
        Task<Boolean> Delete(String wIDNumber);
        Task<Boolean> Delete(String wIDNumber, String palletID, String partNumber,int ordinal, String lotID, String idCode);
    }
}
