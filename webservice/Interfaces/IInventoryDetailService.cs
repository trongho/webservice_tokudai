﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface IInventoryDetailService
    {
        Task<List<InventoryDetail>> GetAll();
        Task<List<InventoryDetail>> GetUnderPartNumber(string partNumber);
        Task<List<InventoryDetail>> GetUnderIDCode(string idCode);
        Task<List<InventoryDetail>> GetUnderId(string warehouseID);
        Task<List<InventoryDetail>> GetUnderWarehouseAnPallet(Int16 year, Int16 month, string warehouseID, string palletID);
        Task<List<InventoryDetail>> GetUnderId(string warehouseID, string partNumber, string palletID,string idCode);
        Task<List<InventoryDetail>> GetUnderId(Int16 year, Int16 month, string warehouseID);
        Task<List<InventoryDetail>> GetUnderId(Int16 year, Int16 month, string warehouseID,string partNumber);
        Task<List<InventoryDetail>> GetUnderId(Int16 year, Int16 month, string warehouseID, string partNumber,string palletID);
        Task<List<InventoryDetail>> GetUnderId(Int16 year, Int16 month, string warehouseID, string partNumber, string palletID, string lotID);
        Task<List<InventoryDetail>> GetUnderId2(Int16 year, Int16 month, string warehouseID, string partNumber, string palletID,string idCode);
        Task<Boolean> Create(InventoryDetail InventoryDetail);
        Task<Boolean> Update(Int16 year, Int16 month, string warehouseID, string partNumber,string palletID, InventoryDetail InventoryDetail);
        Task<Boolean> Delete(Int16 year, Int16 month, string warehouseID, string partNumber, string palletID,string idCode);
        Task<Boolean> Delete(Int16 year, Int16 month, string warehouseID, string partNumber,string palletID);
        Task<Boolean> Delete(Int16 year, Int16 month, string warehouseID,string partNumber);
        Task<Boolean> Delete(Int16 year, Int16 month, string warehouseID);
        Task<Boolean> DeleteOldDate(String warehouseID);

    }
}
