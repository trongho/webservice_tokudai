﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface IWRDataDetailService
    {
        Task<List<WRDataDetail>> GetUnderId(String wRDNumber, String palletID);
        Task<List<WRDataDetail>> GetUnderId(String wRDNumber);
    }
}
