﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface ITSHeaderService
    {
        Task<Boolean> Create(TSHeader entry);
        Task<List<TSHeader>> GetAll();
        Task<List<TSHeader>> GetUnderId(String id);
        Task<List<TSHeader>> GetUnderTSDate(DateTime tsDate);
        Task<Boolean> Update(String id, TSHeader entry);
        Task<Boolean> Delete(String id);
        Task<String> GetLastId();
        Task<List<TSHeader>> GetUnderDate(DateTime fromDate, DateTime toDate);
        Task<List<TSHeader>> GetUnderBranchID(String branchID);
        Task<List<TSHeader>> GetUnderHandlingStatusID(String handlingStatusID);
    }
}
