﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface IUserRightOnScreenService
    {
        Task<List<UserRightOnScreen>> GetRightByUserId(String id);
        Task<List<UserRightOnScreen>> GetRightByScreenId(String id);
        Task<List<UserRightOnScreen>> GetRightByUserScreentId(String userId, String screenId);
        Task<Boolean> CreateUserRightOnScreen(UserRightOnScreen userRightOnScreen);
        Task<Boolean> UpdateUserRightOnScreen(String userId, String screenId, UserRightOnScreen userRightOnScreen);
        Task<Boolean> DeleteUserRightOnScreen(String userId, String screenId);
        Task<Boolean> checkExist(String userId,String screenId);
    }
}
