﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface IInventoryOfLastMonthDetailService
    {
        Task<List<InventoryOfLastMonthDetail>> GetAll();
        Task<List<InventoryOfLastMonthDetail>> GetUnderId(string warehouseID, string partNumber, string palletID, string idCode);
        Task<List<InventoryOfLastMonthDetail>> GetUnderId(Int16 year, Int16 month, string warehouseID);
        Task<List<InventoryOfLastMonthDetail>> GetUnderId(Int16 year, Int16 month, string warehouseID, string partNumber);
        Task<List<InventoryOfLastMonthDetail>> GetUnderId(Int16 year, Int16 month, string warehouseID, string partNumber, string palletID);
        Task<List<InventoryOfLastMonthDetail>> GetUnderId(Int16 year, Int16 month, string warehouseID, string partNumber, string palletID, string lotID);
        Task<List<InventoryOfLastMonthDetail>> GetUnderId2(Int16 year, Int16 month, string warehouseID, string partNumber, string palletID, string idCode);
        Task<Boolean> Create(InventoryOfLastMonthDetail InventoryOfLastMonthDetail);
        Task<Boolean> Update(Int16 year, Int16 month, string warehouseID, string partNumber, string palletID, InventoryOfLastMonthDetail InventoryOfLastMonthDetail);
        Task<Boolean> Delete(Int16 year, Int16 month, string warehouseID, string partNumber, string palletID, string idCode);
        Task<Boolean> Delete(Int16 year, Int16 month, string warehouseID, string partNumber, string palletID);
        Task<Boolean> Delete(Int16 year, Int16 month, string warehouseID, string partNumber);
        Task<Boolean> Delete(Int16 year, Int16 month, string warehouseID);
        Task<Boolean> DeleteOldDate(String warehouseID);
    }
}
