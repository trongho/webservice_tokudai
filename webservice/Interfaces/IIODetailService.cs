﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface IIODetailService
    {
       
        Task<List<IODetail>> GetUnderId(String id);
        Task<List<IODetail>> GetUnderId(String IONumber, String palletID,String partNumber);
        Task<Boolean> Create(IODetail iODetail);
        Task<Boolean> Update(String IONumber,String palletID, String partNumber, int Ordinal, IODetail iODetail);
        Task<Boolean> Delete(String IONumber);
        Task<Boolean> Delete(String IONumber, String palletID, String partNumber, int Ordinal);
    }
}
