﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface ILabelService
    {
        Task<List<Label>> GetAll();
        Task<List<Label>> GetUnderId(String palletID);
        Task<Boolean> Create(Label Label);
        Task<Boolean> Update(String palletID, Label Label);
        Task<Boolean> Delete(String palletID);
        Task<Boolean> Delete();
    }
}
