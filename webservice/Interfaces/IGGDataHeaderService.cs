﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface IGGDataHeaderService
    {
        Task<List<GGDataHeader>> GetAll();
        Task<List<GGDataHeader>> GetUnderId(String id);
        Task<Boolean> Create(GGDataHeader entry);
        Task<Boolean> Update(String id, GGDataHeader entry);
        Task<Boolean> Delete(String id);
        Task<String> GetLastId();
        Task<Boolean> checkExist(String id);

        Task<List<GGDataHeader>> GetUnderDate(DateTime fromDate, DateTime toDate);
        Task<List<GGDataHeader>> GetUnderBranchID(String branchID);
        Task<List<GGDataHeader>> GetUnderHandlingStatusID(String handlingStatusID);

        Task<List<GGDataHeader>> GetUnderOrderStatusID(String orderStatusID);
    }
}
