﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface IWRDataGeneralService
    {
        Task<List<WRDataGeneral>> GetAll();
        Task<List<WRDataGeneral>> GetUnderId(String wRDNumber);
        Task<List<WRDataGeneral>> GetUnderId(String wRDNumber, String palletID, int Ordinal,String lotID);
        Task<List<WRDataGeneral>> GetUnderId(String wRDNumber, String palletID, int Ordinal);
        Task<List<WRDataGeneral>> GetUnderId(String wRDNumber, String palletID);
        Task<List<WRDataGeneral>> GetUnderId(String wRDNumber, String palletID,String lotID);
        Task<Boolean> Create(WRDataGeneral wRDataGeneral);
        Task<Boolean> Update(String wRDNumber,String palletID,int Ordinal,String lotID, WRDataGeneral wRDataGeneral);
        Task<Boolean> Delete(String wRDNumber);
        Task<Boolean> Delete(String wRDNumber,String palletID,String lotID);
        Task<Boolean> Delete(String wRDNumber, String palletID);
    }
}
