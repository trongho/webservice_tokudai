﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface IPalletHeaderService
    {
        Task<List<PalletHeader>> GetAll();
        Task<List<PalletHeader>> GetUnderCreatedDate(DateTime fromDate,DateTime toDate);
        Task<List<PalletHeader>> GetUnderId(String id);
        Task<List<PalletHeader>> GetUnderStatus(String status);
        Boolean Create(PalletHeader PalletHeader);
        Boolean Update(String id,int ordinal, PalletHeader PalletHeader);
        Boolean Delete(String id);
        string GetLastId(string S);
    }
}
