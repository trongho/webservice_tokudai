﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface IMaterialService
    {
        Task<List<Material>> Get();
        Task<List<Material>> Get(string id);
        bool Delete();
        bool Delete(string id);
        bool Create(Material material);
        bool Update(Material material, string id);
    }
}
