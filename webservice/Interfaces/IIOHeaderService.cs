﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface IIOHeaderService
    {
        Task<List<IOHeader>> GetAll();
        Task<List<IOHeader>> GetUnderId(String id);
        Task<List<IOHeader>> GetUnderMonthYear(String monthYear);
        Task<List<IOHeader>> GetUnderLastMonthYear(String monthYear);
        Task<Boolean> Create(IOHeader IOHeader);
        Task<Boolean> Update(String id, IOHeader IOHeader);
        Task<Boolean> Delete(String id);
        Task<String> GetLastId();
        Task<Boolean> checkExist(String id);
        Task<List<IOHeader>> GetUnderDate(DateTime fromDate, DateTime toDate);
        Task<List<IOHeader>> GetUnderBranchID(String branchID);
        Task<List<IOHeader>> GetUnderHandlingStatusID(String handlingStatusID);
        Task<List<IOHeader>> GetUnderModalityID(String modalityID);
    }
}
