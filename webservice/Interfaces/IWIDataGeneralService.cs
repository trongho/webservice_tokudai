﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface IWIDataGeneralService
    {
        Task<List<WIDataGeneral>> GetUnderId(String wIDNumber);
        Task<List<WIDataGeneral>> GetUnderId(String wIDNumber,String palletID,String partNumber);
        Task<Boolean> Create(WIDataGeneral WIDataGeneral);
        
        Task<Boolean> Update(String wIDNumber, String palletID, String partNumber, int Ordinal,WIDataGeneral WIDataGeneral);
        Task<Boolean> Delete(String wIDNumber);
        Task<Boolean> Delete(String wIDNumber,String palletID, String partNumber, int ordinal);
    }
}
