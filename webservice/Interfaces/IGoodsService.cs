﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface IGoodsService
    {
        Task<List<Goods>> GetAllGoods();
        Task<List<Goods>> GetUnderId(String partNumber,String palletID);
        Task<List<Goods>> GetUnderId(String palletID);
        Task<List<Goods>> GetUnderId2(String partNumber);
        Task<Boolean> Create(Goods goods);
        Task<Boolean> Update(String partNumber, String palletID, Goods goods);
        Task<Boolean> Delete(String partNumber, String palletID);
        Task<Boolean> Delete();
    }
}
