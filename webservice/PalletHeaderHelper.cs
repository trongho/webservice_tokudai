﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class PalletHeaderHelper
    {
        public static List<PalletHeaderModel> Covert(List<PalletHeader> entrys)
        {
            var models = entrys.ConvertAll(sc => new PalletHeaderModel
            {
                Ordinal = sc.Ordinal,
                PalletID = sc.PalletID,
                TotalIDCode=sc.TotalIDCode,
                Status = sc.Status,
                HandlingStatusID=sc.HandlingStatusID,
                HandlingStatusName=sc.HandlingStatusName,
                CreatedUserID=sc.CreatedUserID,
                CreatedDate=sc.CreatedDate,
                UpdatedUserID=sc.UpdatedUserID,
                UpdatedDate=sc.UpdatedDate
            });

            return models;
        }
    }
}
