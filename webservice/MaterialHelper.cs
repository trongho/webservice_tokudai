﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class MaterialHelper
    {
        public static List<MaterialModel> Covert(List<Material> entrys)
        {
            var models = entrys.ConvertAll(sc => new MaterialModel
            {
                MaterialID=sc.MaterialID,
                MaterialName=sc.MaterialName
            });

            return models;
        }
    }
}
