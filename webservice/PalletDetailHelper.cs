﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class PalletDetailHelper
    {
        public static List<PalletDetailModel> Covert(List<PalletDetail> entrys)
        {
            var models = entrys.ConvertAll(sc => new PalletDetailModel
            {
                Ordinal = sc.Ordinal,
                PalletID = sc.PalletID,
                IDCode = sc.IDCode,
                PartNumber = sc.PartNumber,
                ProductFamily = sc.ProductFamily,
                LotID = sc.LotID,
                PartName = sc.PartName,
                Unit = sc.Unit,
                Quantity = sc.Quantity,
                MFGDate = sc.MFGDate,
                EXPDate = sc.EXPDate,
                ReceiptDate=sc.ReceiptDate,
                Status = sc.Status,
            });

            return models;
        }
    }
}
