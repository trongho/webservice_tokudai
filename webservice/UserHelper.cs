﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class UserHelper
    {
        public static List<UserModel> CovertUsers(List<User> users)
        {
            var userModels = users.ConvertAll(sc => new UserModel
            {
                UserID = sc.UserID,
                UserName = sc.UserName,
                Adress = sc.Adress,
                Position = sc.Position,
                BranchID = sc.BranchID,
                BranchName = sc.BranchName,
                Email = sc.Email,
                LoginName = sc.LoginName,
                Password = sc.Password,
                PasswordSalt = sc.PasswordSalt,
                Description = sc.Description,
                Status = sc.Status,
                CreatedUserID = sc.CreatedUserID,
                CreatedDate = sc.CreatedDate,
                UpdatedUserID = sc.UpdatedUserID,
                UpdatedDate = sc.UpdatedDate,
                Active = sc.Active,
                Blocked = sc.Blocked,
                Role = sc.Role,
                ControlAllBranches = sc.ControlAllBranches,
                ControlAllSystems = sc.ControlAllSystems,
                ApproveWRLevel1 = sc.ApproveWRLevel1,
                ApproveWRLevel2 = sc.ApproveWRLevel2,
                CancelWR = sc.CancelWR,
                AdjustWR = sc.AdjustWR,
                ApproveIOLevel1 = sc.ApproveIOLevel1,
                ApproveIOLevel2 = sc.ApproveIOLevel2,
                CancelIO = sc.CancelIO,
                AdjustIO = sc.AdjustIO,
                ProcessInventory = sc.ProcessInventory,
                ApproveWRRLevel1 = sc.ApproveWRRLevel1,
                ApproveWRRLevel2 = sc.ApproveWRRLevel2,
                CancelWRR = sc.CancelWRR,
                AdjustWRR = sc.AdjustWRR,
                ApproveWIRLevel1 = sc.ApproveWIRLevel1,
                ApproveWIRLevel2 = sc.ApproveWIRLevel2,
                CancelWIR = sc.CancelWIR,
                AdjustWIR = sc.AdjustWIR,
                ApproveWRDLevel1 = sc.ApproveWRDLevel1,
                ApproveWRDLevel2 = sc.ApproveWRDLevel2,
                CancelWRD = sc.CancelWRD,
                AdjustWRD = sc.AdjustWRD,
                ApproveWIDLevel1 = sc.ApproveWIDLevel1,
                ApproveWIDLevel2 = sc.ApproveWIDLevel2,
                CancelWID = sc.CancelWID,
                AdjustWID = sc.AdjustWID,
                AdjustReceiptQuantity = sc.AdjustReceiptQuantity,
                AdjustIssueQuantity = sc.AdjustIssueQuantity,
                ApprovePalletDataLevel1=sc.ApprovePalletDataLevel1,
                ApprovePalletDataLevel2=sc.ApprovePalletDataLevel2,
                CancelPalletData=sc.CancelPalletData,
                AdjustPalletData=sc.AdjustPalletData,
            });

            return userModels;
        }
    }
}
