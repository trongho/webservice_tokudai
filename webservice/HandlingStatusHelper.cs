﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class HandlingStatusHelper
    {
        public static List<HandlingStatusModel> Covert(List<HandlingStatus> entrys)
        {
            var models = entrys.ConvertAll(sc => new HandlingStatusModel
            {
                HandlingStatusID=sc.HandlingStatusID,
                HandlingStatusName=sc.HandlingStatusName,
                HandlingStatusNameEN=sc.HandlingStatusNameEN,

            });

            return models;
        }
    }
}
