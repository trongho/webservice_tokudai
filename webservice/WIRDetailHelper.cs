﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class WIRDetailHelper
    {
        public static List<WIRDetailModel> Covert(List<WIRDetail> entrys)
        {
            var models = entrys.ConvertAll(sc => new WIRDetailModel
            {
                WIRNumber = sc.WIRNumber,
                Ordinal = sc.Ordinal,
                PalletID = sc.PalletID,
                PartNumber = sc.PartNumber,
                PartName = sc.PartName,
                ProductFamily = sc.ProductFamily,
                LotID = sc.LotID,
                QuantityByItem = sc.QuantityByItem,
                QuantityByPack = sc.QuantityByPack,
                PackingVolume = sc.PackingVolume,
                TotalQuantity = sc.TotalQuantity,
                Unit = sc.Unit,
                MFGDate = sc.MFGDate,
                EXPDate = sc.EXPDate,
                InputDate=sc.InputDate,
                PONumber = sc.PONumber,
                Remark = sc.Remark,
                Status = sc.Status,
                Material=sc.Material,
                Mold=sc.Mold
            });

            return models;
        }
    }
}
