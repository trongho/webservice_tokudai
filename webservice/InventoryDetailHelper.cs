﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class InventoryDetailHelper
    {
        public static List<InventoryDetailModel> Covert(List<InventoryDetail> entrys)
        {
            var models = entrys.ConvertAll(sc => new InventoryDetailModel
            {
                Year = sc.Year,
                Month = sc.Month,
                WarehouseID = sc.WarehouseID,
                PartNumber = sc.PartNumber,
                Status = sc.Status,
                IDCode = sc.IDCode,
                PalletID = sc.PalletID,
                Quantity=sc.Quantity,
                LotID=sc.LotID,
                MFGDate = sc.MFGDate,
                EXPDate = sc.EXPDate,
                InputDate = sc.InputDate,
            });

            return models;
        }
    }
}
