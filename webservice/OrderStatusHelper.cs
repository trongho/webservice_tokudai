﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class OrderStatusHelper
    {
        public static List<OrderStatusModel> Covert(List<OrderStatus> entrys)
        {
            var models = entrys.ConvertAll(sc => new OrderStatusModel
            {
                OrderStatusID = sc.OrderStatusID,
                OrderStatusName = sc.OrderStatusName,
                OrderStatusNameEN = sc.OrderStatusNameEN,

            });

            return models;
        }
    }
}
