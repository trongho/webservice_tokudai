﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class ReportHelper
    {
        public static List<ReportModel> Covert(List<Report> entrys)
        {
            var models = entrys.ConvertAll(sc => new ReportModel
            {
                ReportID=sc.ReportID,
                ReportName=sc.ReportName,
                ReportNameEN=sc.ReportNameEN,
                Ordinal=sc.Ordinal,

            });

            return models;
        }
    }
}
