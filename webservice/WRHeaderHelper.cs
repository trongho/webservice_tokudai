﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class WRHeaderHelper
    {
        public static List<WRHeaderModel> Covert(List<WRHeader> entrys)
        {
            var models = entrys.ConvertAll(sc => new WRHeaderModel
            {
                WRNumber = sc.WRNumber,
                ModalityID = sc.ModalityID,
                ModalityName = sc.ModalityName,
                WRDate = sc.WRDate,
                WRRNumber = sc.WRRNumber,
                WarehouseID = sc.WarehouseID,
                WarehouseName = sc.WarehouseName,
                HandlingStatusID = sc.HandlingStatusID,
                HandlingStatusName = sc.HandlingStatusName,
                BranchID = sc.BranchID,
                BranchName = sc.BranchName,
                TotalQuantity = sc.TotalQuantity,
                Note = sc.Note,
                Status = sc.Status,
                CreatedUserID = sc.CreatedUserID,
                CreatedDate = sc.CreatedDate,
                UpdatedUserID = sc.UpdatedUserID,
                UpdatedDate = sc.UpdatedDate,
            });

            return models;
        }       
    }
}
