﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class TSDataGeneralHelper
    {
        public static List<TSDataGeneralModel> Covert(List<TSDataGeneral> entrys)
        {
            var models = entrys.ConvertAll(sc => new TSDataGeneralModel
            {
              BranchID=sc.BranchID,
              WarehouseID=sc.WarehouseID,
              WarehouseName=sc.WarehouseName,
              TallyDate=sc.TallyDate,
              PartNumber=sc.PartNumber,
              PartName=sc.PartName,
              Ordinal=sc.Ordinal,
              StockUnitID=sc.StockUnitID,
              TallyQuantity=sc.TallyQuantity,
              LedgerQuantity=sc.LedgerQuantity,
              DifferenceQuantity=sc.DifferenceQuantity,
              Status=sc.Status,

            });

            return models;
        }
    }
}
