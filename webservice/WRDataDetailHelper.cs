﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class WRDataDetailHelper
    {
        public static List<WRDataDetailModel> Covert(List<WRDataDetail> entrys)
        {
            var models = entrys.ConvertAll(sc => new WRDataDetailModel
            {
                WRDNumber = sc.WRDNumber,
                Ordinal = sc.Ordinal,
                PalletID = sc.PalletID,
                IDCode = sc.IDCode,
                QuantityOrg = sc.QuantityOrg,
                Quantity = sc.Quantity,
            });

            return models;
        }
    }
}
