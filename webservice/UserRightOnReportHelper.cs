﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class UserRightOnReportHelper
    {
        public static List<UserRightOnReportModel> Covert(List<UserRightOnReport> entrys)
        {
            var models = entrys.ConvertAll(sc => new UserRightOnReportModel
            {
                ReportID = sc.ReportID,
                ReportName = sc.ReportName,
                ReportNameEN = sc.ReportNameEN,
                Ordinal = sc.Ordinal,
                UserID=sc.UserID,
                ViewRight=sc.ViewRight,
                PrintRight=sc.PrintRight,
            });

            return models;
        }
    }
}
